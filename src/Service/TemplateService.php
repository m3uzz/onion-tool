<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Service;


class TemplateService
{

    /**
     * 
     * @param string $psControllerName
     * @param string $psControllerRoute
     * @return string
     */   
    public static function controller (string $psControllerName, string $psControllerRoute) : string
    {
        $lsController = "Controller\\{$psControllerName}Controller::class => [
                            'alias' => '{$psControllerName}',
                            'title' => '{$psControllerName}',
                            'description' => '',
                            'icon' => 'material-icons extension',
                            'default-route' => '{$psControllerRoute}',
                            'default-action' => '{$psControllerRoute}',
                            'change-permission' => true,
                            'actions' => [
                                'index' => [
                                    'title' => 'Ativos',
                                    'description' => 'Listar registros ativos',
                                    'icon' => 'material-icons inbox',
                                    'route' => '/{$psControllerRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'trash' => [
                                    'title' => 'Lixeira',
                                    'description' => 'Listar registros desativados na lixeira',
                                    'icon' => 'material-icons folder_delete',
                                    'route' => '/{$psControllerRoute}/trash',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'add' => [
                                    'title' => 'Adicionar',
                                    'description' => 'Adicionar novo registro',
                                    'icon' => 'material-icons add',
                                    'route' => '/{$psControllerRoute}/add',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'edit' => [
                                    'title' => 'Editar',
                                    'description' => 'Editar dados do registro',
                                    'icon' => 'material-icons edit',
                                    'route' => '/{$psControllerRoute}/edit',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'save-grid-field' => [
                                    'title' => 'Salvar campo',
                                    'description' => 'Editar dados do registro',
                                    'icon' => 'material-icons done',
                                    'route' => '/{$psControllerRoute}/save-grid-field',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'duplicate' => [
                                    'title' => 'Duplicar',
                                    'description' => 'Duplicar registro',
                                    'icon' => 'material-icons content_copy',
                                    'route' => '/{$psControllerRoute}/duplicate',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'view' => [
                                    'title' => 'Visualizar',
                                    'description' => 'Visualizar detalhes do registro',
                                    'icon' => 'material-icons visibility',
                                    'route' => '/{$psControllerRoute}/view',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'move' => [
                                    'title' => 'Mover',
                                    'description' => 'Mover registro para um outra pasta',
                                    'icon' => 'material-icons drive_file_move',
                                    'route' => '/{$psControllerRoute}/move',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'desactive' => [
                                    'title' => 'Desativar',
                                    'description' => 'Desativar retistro enviando para a lixeira',
                                    'icon' => 'material-icons delete',
                                    'route' => '/{$psControllerRoute}/desactive',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'restore' => [
                                    'title' => 'Restaurar',
                                    'description' => 'Restaurar registro da lixeira para ativos',
                                    'icon' => 'material-icons undo',
                                    'route' => '/{$psControllerRoute}/restore',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'delete' => [
                                    'title' => 'Apagar',
                                    'description' => 'Apagar registro permanentemente',
                                    'icon' => 'material-icons delete_forever',
                                    'route' => '/{$psControllerRoute}/delete',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'search-window' => [
                                    'title' => 'Janela de busca',
                                    'description' => 'Acessar janela de busca de registros',
                                    'icon' => 'material-icons pageview',
                                    'route' => '/{$psControllerRoute}/search-window',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'search' => [
                                    'title' => 'Campo busca',
                                    'description' => 'Campo com busca e preenchimento automático em formulário',
                                    'icon' => 'material-icons search',
                                    'route' => '/{$psControllerRoute}/search',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'reload-input' => [
                                    'title' => 'Carregar campo do form',
                                    'description' => 'Carregar campo único do form',
                                    'icon' => 'material-icons restart_alt',
                                    'route' => '/{$psControllerRoute}/reload-input',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'csv' => [
                                    'title' => 'Exportar CSV',
                                    'description' => 'Exportar registros marcados para arquivo CSV',
                                    'icon' => 'material-icons file_download',
                                    'route' => '/{$psControllerRoute}/csv',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'pdf' => [
                                    'title' => 'Exportar PDF',
                                    'description' => 'Exportar registros marcados para arquivo PDF',
                                    'icon' => 'material-icons file_download',
                                    'route' => '/{$psControllerRoute}/pdf',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'xls' => [
                                    'title' => 'Exportar XLS',
                                    'description' => 'Exportar registros marcados para arquivo XLS',
                                    'icon' => 'material-icons file_download',
                                    'route' => '/{$psControllerRoute}/xls',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],                
                                /*controllerAction{$psControllerName}*/
                            ]
                        ],
                        /*moduleController*/";

        return $lsController;
    }


    /**
     * 
     * @param string $psControllerName
     * @param string $psControllerRoute
     * @return string
     */   
    public static function controllerRestful (string $psControllerName, string $psControllerRoute) : string
    {
        $lsController = "Controller\\{$psControllerName}RestfulController::class => [
                            'alias' => '{$psControllerName}Restful',
                            'title' => '{$psControllerName}',
                            'description' => '',
                            'icon' => 'material-icons api',
                            'default-route' => '{$psControllerRoute}-restful',
                            'default-action' => '/api/{$psControllerRoute}',
                            'change-permission' => true,
                            'actions' => [
                                'get' => [
                                    'title' => 'Get',
                                    'description' => '',
                                    'icon' => 'material-icons swap_vert',
                                    'route' => '/api/{$psControllerRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'post' => [
                                    'title' => 'Create',
                                    'description' => '',
                                    'icon' => 'material-icons post_add',
                                    'route' => '/api/{$psControllerRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'put' => [
                                    'title' => 'Update',
                                    'description' => '',
                                    'icon' => 'material-icons edit',
                                    'route' => '/api/{$psControllerRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                'delete' => [
                                    'title' => 'Delete',
                                    'description' => '',
                                    'icon' => 'material-icons delete_forever',
                                    'route' => '/api/{$psControllerRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],                                
                                /*controllerAction{$psControllerName}Restful*/
                            ]
                        ],
                        /*moduleController*/";

        return $lsController;
    }


    /**
     * 
     * @param string $psControllerRoute
     * @param string $psModuleName
     * @param string $psControllerName
     * @return string
     */    
    public static function route (string $psControllerRoute, string $psModuleName, string $psControllerName) : string
    {
        $lsRoute = "'{$psControllerRoute}' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/{$psControllerRoute}[/:action][/:id][/:query][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'module' => '{$psModuleName}',
                        'ctrl-alias' => '{$psControllerName}',
                        'controller' => Controller\\{$psControllerName}Controller::class,
                        'action' => 'index'
                    ],
                ]
            ],
            /*routes*/";

        return $lsRoute;
    }


    /**
     * 
     * @param string $psControllerRoute
     * @param string $psModuleName
     * @param string $psControllerName
     * @return string
     */    
    public static function routeRestful (string $psControllerRoute, string $psModuleName, string $psControllerName) : string
    {
        $lsRoute = "'{$psControllerRoute}-restful' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/{$psControllerRoute}[/:id][/]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'restful' => true,
                        'module' => '{$psModuleName}',
                        'ctrl-alias' => '{$psControllerName}Restful',
                        'controller' => Controller\\{$psControllerName}RestfulController::class,
                    ],
                ]
            ],
            /*routes*/";

        return $lsRoute;
    }    


    /**
     * 
     * @param string $psModuleName
     * @param string $psControllerName
     * @param string $psActionName
     * @return string
     */    
    public static function routeConsole (string $psModuleName, string $psControllerName, string $psActionName) : string
    {
        $lsRoute = "'{$psActionName}' => [
                    'type' => 'simple', // 'catchall',
                    'options' => [
                        'route' => '{$psActionName} [--test] [--json]',
                        'defaults' => [
                            'module' => '{$psModuleName}',
                            'ctrl-alias' => '{$psControllerName}Console',
                            'controller' => Controller\\{$psControllerName}ConsoleController::class,
                            'action' => '{$psActionName}'
                        ],
                    ]
                ],
                /*consoleroutes*/";

        return $lsRoute;
    }


    /**
     * 
     * @param string $psActionRoute
     * @param string $psActionTitle
     * @param string $psRoute
     * @param string $psControllerName
     * @param string $psRestfulSufix
     * @return string
     */
    public static function action (string $psActionRoute, string $psActionTitle, string $psRoute, string $psControllerName, string $psRestfulSufix) : string
    {
        $lsAction = "'{$psActionRoute}' => [
                                    'title' => '{$psActionTitle}',
                                    'description' => '',
                                    'icon' => 'material-icons rocket_launch',
                                    'route' => '{$psRoute}',
                                    'access-key' => '',
                                    'help' => '',
                                    'change-permission' => true
                                ],
                                /*controllerAction{$psControllerName}{$psRestfulSufix}*/";

        return $lsAction;
    }


    /**
     * 
     * @param string $psActionName
     * @return string
     */
    public static function actionMethod (string $psActionName) : string
    {
        $lsActionMethod = "\n
        /**
         *
         * @return \Laminas\View\Model\ViewModel
         */
        public function {$psActionName}Action ()
        {
            Debug::debug(__METHOD__);
            
            \$lsAction = \$this->requestParam('action');
    
            //\$this->oService->{$psActionName}();

            \$loView = new ViewModel();
    
            \$loView->setVariables([
                'laModuleProp' => \$this->aModuleProp,
                'laCtrlConf' => \$this->aCtrlConfig,
                'lsAction' => \$lsAction
            ]);
            
            return \$this->setResponseType(\$loView, null, true);
        }\n}\n";

        return $lsActionMethod;
    }


    /**
     * 
     * @param string $psActionName
     * @param string $psMethod
     * @return string
     */
    public static function methodHandler (string $psActionName, string $psMethod) : string
    {
        $lsMethodHandler = "
		\$this->addHttpMethodHandler(\"{$psMethod}\", function (\$poEvent)
		{
			return \$this->{$psActionName}(\$poEvent);
		});

		/*addHttpMethodHandler*/";

        return $lsMethodHandler;
    }


    /**
     * 
     * @param string $psActionName
     * @param string $psMethod
     * @param string $psRoute
     * @param string $psControllerName
     * @return string
     */
    public static function actionMethodRestful (string $psActionName, string $psMethod, string $psRoute, string $psControllerName) : string
    {
        $lsActionMethod = "\n
        /**
         * @SWG\\{$psMethod}(
         *   path=\"/{$psRoute}\",
         *   tags={\"{$psControllerName}\"},
         *   summary=\"Api summary\",
         *   description=\"Api description\",
         *   operationId=\"{$psMethod}{$psActionName}\",
         *   @SWG\Parameter(
         *     name=\"Param\",
         *     type=\"array\",
         *     in=\"body\",
         *     required=true,
         *     description=\"Param description\",
         *     @SWG\Schema(
         *       @SWG\Property(
         *         property=\"ParamName1\",
         *         type=\"string\",
         *         format=\"email\",
         *         required=true
         *       ),
         *       @SWG\Property(
         *         property=\"ParamName2\",
         *         type=\"string\",
         *         required=true
         *       )
         *     )
         *   ),
         *   @SWG\Response(
         *     response=200,
         *     description=\"Response description\",
         *     ref=\"\"
         *   ),
         *   @SWG\Response(
         *     response=\"default\",
         *     description=\"an \"\"unexpected\"\" error\"
         *   )
         * )
         * 		
         * @param object \$poEvent
         * @return array
         */
        public function {$psActionName} (\$poEvent)
        {
            Debug::debug(__METHOD__);
            
            //\$this->oService->{$psActionName}();

            \$loRequest = \$this->getRequest();
                
            if (\$this->requestHasContentType(\$loRequest, self::CONTENT_TYPE_JSON)) 
            {
                \$laData = json_decode(\$loRequest->getContent(), true);
            }
            else
            {
                \$laData = \$loRequest->getPost()->toArray();
            }
            
            \$laResponse = [
                'status' => 'success',
                'statusMessage' => [
                    'method' => \$loRequest->getMethod(),
                    'code' => 200,
                    'message' => ['ok']
                ],
                'data' => []
            ];
    
            \$loView = new JsonModel(\$laResponse);
    
            return \$this->setResponseType(\$loView, null, \$laResponse['statusMessage']['code']);
        }\n}\n";

        return $lsActionMethod;
    }


    /**
     * 
     * @param string $psActionName
     * @return string
     */
    public static function actionMethodConsole (string $psActionName) : string
    {
        $lsActionMethod = "\n
        /**
         */
        public function {$psActionName}Action ()
        {
            Debug::debug(__METHOD__);
            
            \$lbJson = \$this->request('json');
            \$lbTest = \$this->request('test');
    
            //\$this->oService->{$psActionName}(\$lbTest);
            
            \$laResponse = \$this->oService->getResponse();
                
            if (\$lbJson && is_array(\$laResponse))
            {
                echo json_encode(\$laResponse);
            }
        }\n}\n";

        return $lsActionMethod;
    }


    /**
     * 
     * @param string $psField
     * @param string $psRequired
     * @return string
     */
    public static function formField (string $psField, string $psRequired) : string
    { 
        $lsField = "\$loInput = \$this->addInput('{$psField}', '{$psField}');\n";
        $lsField .= "\t\t\$loInput->setAttributes([\n";
        $lsField .= "\t\t\t\t'title' => Translator::i18n(''),\n";
        $lsField .= "\t\t\t\t'required' => {$psRequired},\n";
        $lsField .= "\t\t\t\t'placeholder' => Translator::i18n(''),\n";
        $lsField .= "\t\t\t\t'data-mask' => '',\n";
        $lsField .= "\t\t\t\t'data-mask-alt' => '',\n";
        $lsField .= "\t\t\t\t'pattern' => '',\n";
        $lsField .= "\t\t]);\n";
        $lsField .= "\t\t\$loInput->setOption('row_attributes', [\n";
        $lsField .= "\t\t\t\t'class' => 'input-form col-lg-6'\n";
        $lsField .= "\t\t]);\n\n\n\t\t";

        return $lsField;
    }


    /**
     * 
     * @param string $psField
     * @return string
     */
    public static function gridCol (string $psField) : string
    { 
        $lsCol = "\$this->createColumn('{$psField}')\n";
        $lsCol .= "\t\t\t->setTitle(Translator::i18n('{$psField}'))\n";
        $lsCol .= "\t\t\t->setDescription()\n";
        $lsCol .= "\t\t\t->setSearchField('a.{$psField}')\n";
        $lsCol .= "\t\t\t->setVisible(true)\n";
        $lsCol .= "\t\t\t->setSortable(true)\n";
        $lsCol .= "\t\t\t->setResizable(true)\n";
        $lsCol .= "\t\t\t->setEditable(false)\n";
        $lsCol .= "\t\t\t->setAlign('left')\n";
        $lsCol .= "\t\t\t->setWidth('')\n";
        $lsCol .= "\t\t\t->setFormat(function (\$poColumn, \$paRow) {});\n\n\n\t\t";

        return $lsCol;
    }
}