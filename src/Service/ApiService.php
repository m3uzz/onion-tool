<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Service;
use OnionLib\ArrayObject;
use OnionLib\Prompt;
use OnionLib\Str;
use OnionLib\System;
use OnionLib\Util;
use OnionTool\Repository\InstallRepository;


class ApiService extends ToolServiceAbstract
{
	
	/**
	 * 
	 */
	public function init () : void
	{
		$this->sTemplatePath = dirname(__DIR__) . DS . 'Template' . DS . 'api';
	}
	
	
	/**
	 * 
	 */
	public function newProject () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', "onionapp.com"));
		$this->setModuleName($this->getRequestArg('module', "index"));
		
		$lsPathProject = $this->sProjectPath;
		
		$this->createDir($lsPathProject);
		
		if (file_exists($lsPathProject))
		{
			$lsFileLicense = $this->getLicense($this->sProjectFolder);
		
			$this->saveFile($lsPathProject, 'onionapi', $lsFileLicense);
			$this->saveFile($lsPathProject, 'composer', '', 'json');
			
			$lsPathConfig = $lsPathProject . DS . 'config';			
			$this->createDir($lsPathConfig);
			$this->saveFile($lsPathConfig, 'api-config');
			$this->saveFile($lsPathConfig, 'api-modsAvailable');
			$this->saveFile($lsPathConfig, 'api-module');
			$this->saveFile($lsPathConfig, 'api-service');
			$this->saveFile($lsPathConfig, 'api-validate');
			
			$lsPathData = $lsPathProject . DS . 'data';
			$this->createDir($lsPathData, true, 0777);
			$lsPathDataLogs = $lsPathData . DS . 'logs';
			$this->createDir($lsPathDataLogs, true, 0777);
			
			$lsPathLayout = $lsPathProject . DS . 'layout';
			$this->createDir($lsPathLayout);
			$lsPathLayoutTheme = $lsPathLayout . DS . 'theme';
			$this->createDir($lsPathLayoutTheme);
			$lsPathLayoutTemplate = $lsPathLayout . DS . 'template';
			$this->createDir($lsPathLayoutTemplate);
			
			$lsPathPublic = $lsPathProject . DS . 'public-api';			
			$this->createDir($lsPathPublic, false);
			$this->saveFile($lsPathPublic, 'index', $lsFileLicense);
			$this->saveFile($lsPathPublic, 'htaccess');
			$this->saveFile($lsPathPublic, 'robots', null, 'txt');
			$this->saveFile($lsPathPublic, 'sitemap', null, 'xml');
			
			$lsPathPublicCss = $lsPathPublic . DS . 'css';
			$this->createDir($lsPathPublicCss);
			$lsPathPublicFonts = $lsPathPublic . DS . 'fonts';
			$this->createDir($lsPathPublicFonts);
			$lsPathPublicImg = $lsPathPublic . DS . 'img';
			$this->createDir($lsPathPublicImg);
			$lsPathPublicJs = $lsPathPublic . DS . 'js';
			$this->createDir($lsPathPublicJs);
			
			$lsPathApi = $lsPathProject . DS . 'api';
			$this->createDir($lsPathApi);
		}
		
		if ($this->sModuleName == null)
		{
			$this->setModuleName("Index");
		}
		
		$this->newModule();
		
		if (Util::toBoolean($this->getRequestArg('vhost', 'true', false, "Try create virtual host conf to Apache? (You need to have root access!)")))
		{
			$this->oController->setAction("virtualHostDev");
			$this->virtualHostDev("Api");
		}
		else
		{
			$this->oController->setAction("showVHostConf");
			$this->showVHostConf();
		}
	}
	
	
	/**
	 * 
	 */
	public function newModule () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', "onionapp.com"));
		$this->setModuleName($this->getRequestArg('module', null, true));
		
		if ($this->sModuleName == null)
		{
			Prompt::echoError("The param module is required! Please, use --help for further information.");
			return;
		}
		
		$lsPathProject = $this->sProjectPath;
		$lsPathApi = $lsPathProject . DS . 'api';
		$lsPathConfig = $lsPathProject . DS . 'config';
		
		if (file_exists($lsPathApi))
		{
			$lsPathModule = $lsPathApi . DS . $this->sModuleName;
			$this->createDir($lsPathModule);
			
			if (file_exists($lsPathModule))
			{
				$lsFileLicense = $this->getLicense($this->sModuleName);
				
				$this->saveFile($lsPathModule, 'Module', $lsFileLicense);
				$this->saveFile($lsPathModule, '_composer', '', 'json', '');
				
				$lsPathModuleConfig = $lsPathModule . DS . 'config';
				$this->createDir($lsPathModuleConfig);	
				$this->saveFile($lsPathModuleConfig, 'module-config');
				$this->saveFile($lsPathModuleConfig, 'help');

				$lsModuleRoute = Str::slugfy($this->sModuleName);
				
				$lsPathView = $lsPathModule . DS . 'view';
				$this->createDir($lsPathView);
				$lsPathViewModule = $lsPathView . DS . $lsModuleRoute;
				$this->createDir($lsPathViewModule);
				$lsPathViewController = $lsPathViewModule . DS . $lsModuleRoute;
				$this->createDir($lsPathViewController);
				
				$lsPathSrc = $lsPathModule . DS . 'src';
				$this->createDir($lsPathSrc);
										
				if (file_exists($lsPathSrc))
				{
					$lsPathController = $lsPathSrc . DS . 'Controller';
					$this->createDir($lsPathController);
					$this->saveFile($lsPathController, '_Controller', $lsFileLicense);

					$lsPathService = $lsPathSrc . DS . 'Service';
					$this->createDir($lsPathService);
					$this->saveFile($lsPathService, '_Service', $lsFileLicense);
						
					$lsPathView = $lsPathSrc . DS . 'View';
					$this->createDir($lsPathView);
					$this->saveFile($lsPathView, '_View', $lsFileLicense);
						
					$lsPathEntity = $lsPathSrc . DS . 'Entity';
					$this->createDir($lsPathEntity);
					$this->saveFile($lsPathEntity, 'Entity', $lsFileLicense);
						
					$lsPathRepository = $lsPathSrc . DS . 'Repository';
					$this->createDir($lsPathRepository);
					$this->saveFile($lsPathRepository, '_Repository', $lsFileLicense);
						
					$this->setApiModuleAutoload($lsPathConfig);
				}
			}
		}
		else 
		{
			Prompt::exitError("Project folder do not exist! You need to create a new project first. Please, use --help for further information.");
		}
	}
	
	
	/**
	 *
	 */
	public function newController () : void
	{
		$lsPathModule = $this->getModulePath('api');
		
		$lsControllerName = ucfirst($this->getRequestArg('controller', null, true));
		
		if ($lsControllerName == null)
		{
			Prompt::echoError("The param controller is required! Please, use --help for further information.");
			return;
		}

		$lsFileLicense = $this->getLicense($this->sModuleName);
					
		$lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module-config.php';
				
		$lsModuleRoute = Str::slugfy($this->sModuleName);
		$lsControllerRoute = Str::slugfy($lsControllerName);
				
		if (file_exists($lsPathModuleConfig))
		{
			$laModuleConfig = include($lsPathModuleConfig);
			$lsNamespace = $this->sModuleName . '\\Controller\\' . $lsControllerName;
			$laModuleConfig['controllers']['invokables'][$lsNamespace] = $lsNamespace . "Controller";
				
			$laModuleConfig['router']['routes'][$lsControllerRoute] = [
				'type' => 'Segment',
				'options' => [
					'route' => "/{$lsControllerRoute}[/:action][/:id][/]",
					'constraints' => [
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					],
					'defaults' => [
						'__NAMESPACE__' => $this->sModuleName . '\Controller',
						'controller' => $lsControllerName,
						'action' => 'index',
					],
			]];
				
			$lsFileContent = ArrayObject::arrayToFile($laModuleConfig);
			
			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
				
		$lsPathModuleHelp = $lsPathModule . DS . 'config' . DS . 'help.php';
				
		if (file_exists($lsPathModuleHelp))
		{
			$laModuleHelp = include($lsPathModuleHelp);
			$laModuleHelp[$lsModuleRoute][$lsControllerRoute][$lsControllerRoute] = [
				"desc" => "Action description",
				"params" => [
					"paramName" => "Param value"
				]
			];
						
			$lsFileContent = ArrayObject::arrayToFile($laModuleHelp);
			
			if (System::saveFile($lsPathModuleHelp, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleHelp);
			}
		}
					
		$lsPathView = $lsPathModule . DS . 'view';
		$lsPathViewModule = $lsPathView . DS . $lsModuleRoute;
		$lsPathViewController = $lsPathViewModule . DS . $lsControllerRoute;
		$this->createDir($lsPathViewController);
				
		$lsPathSrc = $lsPathModule . DS . 'src';
				
		if (file_exists($lsPathSrc))
		{
			$lsPathController = $lsPathSrc . DS . 'Controller';
			$this->saveFile($lsPathController, '_Controller', $lsFileLicense, 'php', $lsControllerName);
			
			$lsPathService = $lsPathSrc . DS . 'Service';
			$this->saveFile($lsPathService, '_Service', $lsFileLicense, 'php', $lsControllerName);

			$lsPathView = $lsPathSrc . DS . 'View';
			$this->saveFile($lsPathView, '_View', $lsFileLicense);

			$lsPathEntity = $lsPathSrc . DS . 'Entity';
			$this->saveFile($lsPathEntity, 'Entity', $lsFileLicense);
				
			$lsPathRepository = $lsPathSrc . DS . 'Repository';
			$this->saveFile($lsPathRepository, '_Repository', $lsFileLicense);
		}
	}


	/**
	 *
	 */
	public function newService () : void
	{
		$lsPathModule = $this->getModulePath('api');
		
		$lsServiceName = ucfirst($this->getRequestArg('service', null, true));
		
		if ($lsServiceName == null)
		{
			Prompt::echoError("The param service is required! Please, use --help for further information.");
			return;
		}

		$lsFileLicense = $this->getLicense($this->sModuleName);
					
		$lsPathSrc = $lsPathModule . DS . 'src';
				
		if (file_exists($lsPathSrc))
		{
			$lsPathService = $lsPathSrc . DS . 'Service';
			$this->saveFile($lsPathSrc, '_Service', $lsFileLicense, 'php', $lsServiceName);
		}
	}
	
	
	/**
	 * 
	 */
	public function setEntity () : void
	{
		$lsPathModule = $this->getModulePath('api');
		
		$lsDbPath = 'config' . DS . 'api-service.php';
		$laDbProjectConf = require($this->sProjectPath . DS . $lsDbPath);
		
		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['db']['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['db']['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['db']['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['db']['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['db']['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['db']['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['db']['database'], true);
		
		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,			        
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		    'database' => $lsDbName,
		];
		
		$this->aRepository['Db'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['Db']->connect())
		{
		    $lsTableName = $this->getRequestArg('table', $this->sModuleName, true);
		    
		    $laTable = $this->aRepository['Db']->descEntity($lsTableName);

			if (is_array($laTable))
			{
			    $lsField = "";
			    $lsData = "";
			    
			    foreach ($laTable as $laField)
			    {
			        $lsF = $laField['Field'];
			        
			        if ($lsF != 'id' && $lsF != 'stHash' && $lsF != 'SysEntityAccountOwner_id'  && $lsF != 'SysUserOwner_id' && $lsF != 'dtInsert' && $lsF != 'dtUpdate' && $lsF != 'numStatus' && $lsF != 'isActive')
			        {
    			        $lsDefault = "";
    			        $lsPri = "";
    			        $lsPriORM = "";
    			        $lsAuto = "";
    			        $lsNull = '* @ORM\Column(nullable=true)';    			        
    			        
    			    	if ($laField['Default'] == '0' || !empty($laField['Default']))
    			        {
    			            $lsDefault = " = {$laField['Default']}";
    			        }
    			        
    			        if ($laField['Key'] == 'PRI')
    			        {
    			            $lsPri = " PK";
    			            $lsPriORM = "\n\t" . ' * @ORM\Id';
    			        }
    			        
			            if ($laField['Extra'] == 'auto_increment')
    			        {
    			            $lsAuto = "\n\t" . ' * @ORM\GeneratedValue(strategy="AUTO")';
    			        }
    			        
    			        if ($laField['Null'] == 'NO')
    			        {
    			            $lsNull = ' * @ORM\Column(nullable=false)';
    			        }
    			        
    			        $laType = explode("(", $laField['Type']);
    			        $lsType = $laType[0];
    			        
    			    	switch ($lsType)
    			        {
    			            case 'int':
    			            case 'tinyint':
    			            case 'smallint':
    			            case 'mediumint':
    			            case 'bigint':
    			                $lsFieldType = ' * @var int';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="integer")';
    			                break;
    			            case 'text':
    			            case 'tinytext':
    			            case 'mediumtext':
    			            case 'longtext':
    			            case 'blob':
    			            case 'tinyblob':
    			            case 'mediumblob':
    			            case 'longblob':
    			                $lsFieldType = ' * @var string';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="text")';
    			                break;
    			            case 'date':
    			            case 'time':			                 
    			            case 'datetime':
    			                $lsFieldType = ' * @var string';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="datetime")';
    			                break;
							case 'timestamp':
								$lsFieldType = ' * @var int';
								$lsFieldTypeORM = ' * @ORM\Column(type="integer")';
								break;
							case 'decimal':
    			            case 'float':
    			            case 'double':
    			            case 'real':
    			                $lsFieldType = ' * @var float';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="decimal")';
    			                break;
    			            case 'boolean':
    			                $lsFieldType = ' * @var bool';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="boolean")';
    			                break;
    			            default:
    			                $lsFieldType = ' * @var string';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="string")';
    			        }	
    			        
    			        $lsField .= "\t/**\n\t{$lsFieldType}{$lsPri}{$lsPriORM}{$lsAuto}\n\t{$lsFieldTypeORM}\n\t{$lsNull} */\n\tprotected \${$laField['Field']}{$lsDefault};\n\n";
			        }
			    }
			    
				$lsPathSrcModule = $lsPathModule . DS . 'src';			    
				$lsPathEntity = $lsPathSrcModule . DS . 'Entity';
				$lsPathEntityFile = $lsPathEntity . DS . "{$lsTableName}.php";
				   		    
    		    if (!file_exists($lsPathEntityFile))
    		    {
                    $lsFileLicense = $this->getLicense($this->sModuleName);
					$this->saveFile($lsPathEntity, 'Entity', $lsFileLicense, 'php', $lsTableName);
                }
				
				$lsFileContent = System::localRequest($lsPathEntityFile);

		        Util::parse($lsFileContent, "#%FIELDS%#", $lsField);
		        
		        if (System::saveFile($lsPathEntityFile, $lsFileContent))
		        {
		            $this->changedFileMsg($lsPathEntityFile);
		        }
			}
		}	    
	}	
}