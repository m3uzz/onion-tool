<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Service;
use OnionLib\ArrayObject;
use OnionLib\Debug;
use OnionLib\Prompt;
use OnionLib\Str;
use OnionLib\System;
use OnionLib\Util;
use OnionTool\Repository\InstallRepository;


class ZfsService extends ToolServiceAbstract
{
	
	/**
	 * 
	 */
	public function init () : void
	{
		$this->sTemplatePath = dirname(__DIR__) . DS . 'Template' . DS . 'zfs';
		$this->sDbPath = dirname(__DIR__) . DS . 'Db';
		$this->sLayoutVendor = BASE_DIR . DS . 'layout' . DS . 'vendor';
	}


	/**
	 * 
	 */
	public function newProject () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', "onionapp.com"));

		if (!$this->bIsConsole)
		{
			$this->setProjectDomain($this->getRequestArg('domain', "local.{$this->sProjectFolder}"));
		}

		$this->setProjectName($this->getRequestArg('projectname', "OnionApp"));
		
		$lsPathProject = $this->sProjectPath;
		
		$this->createDir($lsPathProject);
		
		if (file_exists($lsPathProject))
		{
			$lsFileLicense = $this->getLicense($this->sProjectFolder);
			
			$this->saveFile($lsPathProject, 'composer', '', 'json');
			$this->saveFile($lsPathProject, 'testInit');
			$this->saveFile($lsPathProject, 'console');

			$lsPathLanguage = $lsPathProject . DS . 'language';			
			$this->createDir($lsPathLanguage);

			$lsPathConfig = $lsPathProject . DS . 'config';			
			$this->createDir($lsPathConfig);
			$this->saveFile($lsPathConfig, $this->bIsConsole ? 'console-project' : 'project');
			$this->saveFile($lsPathConfig, $this->bIsConsole ? 'console-modules' : 'modules');
			$this->saveFile($lsPathConfig, 'log');
			$this->saveFile($lsPathConfig, 'service');
			$this->saveFile($lsPathConfig, 'db');
			$this->saveFile($lsPathConfig, 'mail');
			$this->saveFile($lsPathConfig, 'table');
			$this->saveFile($lsPathConfig, 'repository');
			$this->saveFile($lsPathConfig, 'business');

			if (!$this->bIsConsole)
			{
				$this->saveFile($lsPathConfig, 'auth');
				$this->saveFile($lsPathConfig, 'roles');
				$this->saveFile($lsPathConfig, 'cache');
				$this->saveFile($lsPathConfig, 'backend');
				$this->saveFile($lsPathConfig, 'frontend');
				$this->saveFile($lsPathConfig, 'layout');
				$this->saveFile($lsPathConfig, 'menu');
				$this->saveFile($lsPathConfig, 'form');
				$this->saveFile($lsPathConfig, 'grid');
				$this->saveFile($lsPathConfig, 'session');

				$lsPathAcl = $lsPathConfig . DS . 'acl';
				$this->createDir($lsPathAcl);
				$this->saveFile($lsPathAcl, 'admin');
				$this->saveFile($lsPathAcl, 'guest');
			}

			$lsPathData = $lsPathProject . DS . 'data';
			$this->createDir($lsPathData, true, 0777);
			$lsPathDataLogs = $lsPathData . DS . 'logs';
			$this->createDir($lsPathDataLogs, true, 0777);
			$lsPathDataTemp = $lsPathData . DS . 'temp';
			$this->createDir($lsPathDataTemp, true, 0777);
			$lsPathDataCache = $lsPathData . DS . 'cache';
			$this->createDir($lsPathDataCache, true, 0777);

			$lsPathTemplate = $lsPathProject . DS . 'template';
			$this->createDir($lsPathTemplate);
			$lsPathTemplateEmail = $lsPathTemplate . DS . 'email';
			$this->createDir($lsPathTemplateEmail);

			if (!$this->bIsConsole)
			{
				$lsPathDataUploads = $lsPathData . DS . 'uploads';
				$this->createDir($lsPathDataUploads, true, 0777);

				$lsPathTemplateTheme = $lsPathTemplate . DS . 'theme';
				$this->createDir($lsPathTemplateTheme);
			
				$lsPathPublic = $lsPathProject . DS . 'public';
				$this->createDir($lsPathPublic, false);
				$this->saveFile($lsPathPublic, 'index', $lsFileLicense);
				$this->saveFile($lsPathPublic, 'htaccess');
				$this->saveFile($lsPathPublic, 'robots', null, 'txt');
				$this->saveFile($lsPathPublic, 'manifest', null, 'json');
				$this->saveFile($lsPathPublic, 'sitemap', null, 'xml');
				
				$lsPathPublicCss = $lsPathPublic . DS . 'css';
				$this->createDir($lsPathPublicCss);
				$lsPathPublicFonts = $lsPathPublic . DS . 'fonts';
				$this->createDir($lsPathPublicFonts);
				$lsPathPublicImg = $lsPathPublic . DS . 'img';
				$this->createDir($lsPathPublicImg);
				$lsPathPublicJs = $lsPathPublic . DS . 'js';
				$this->createDir($lsPathPublicJs);
			}

			$lsPathModule = $lsPathProject . DS . 'module';
			$this->createDir($lsPathModule);
		}
		
		if (!$this->bIsConsole && !$this->bIsRestful)
		{
			$this->newModule("Backend");
		
			$this->newModule("Frontend");
		}

		if (Prompt::confirm("Create new Module?", 'n'))
		{
			$this->setModuleName($this->getRequestArg('module'));
		
			if ($this->sModuleName != 'Backend' && $this->sModuleName != "Frontend")
			{
				$this->oController->setAction("newModule");
				$this->newModule();
			}
		}
		
		if (Prompt::confirm("Configure onion database?"))
		{
			$this->oController->setAction("installDb");
			$this->installDb();
		}
		
		if (!$this->bIsConsole)
		{
			if (Util::toBoolean($this->getRequestArg('vhost', 'true', false, "Try create virtual host conf to Apache? (You need to have root access!)")))
			{
				$this->oController->setAction("virtualHostDev");
				$this->virtualHostDev("Zfs");
			}
			else
			{
				$this->oController->setAction("showVHostConf");
				$this->showVHostConf();
			}
		}
	}
	

	/**
	 * 
	 * @param string|null $psModuleName
	 */
	public function newModule (?string $psModuleName = null) : void
	{
		if ($psModuleName == null)
		{
			$this->setProjectFolder($this->getRequestArg('project', "onionapp.com"));
			$this->setModuleName($this->getRequestArg('module', null, true));
		}
		else 
		{
			$this->setModuleName($psModuleName);
		}	
		
		if ($this->sModuleName == null)
		{
			Prompt::echoError("The param module is required! Please, use --help for further information.");
			return;
		}
		
		$lsPathProject = $this->sProjectPath;
		$lsPathModules = $lsPathProject . DS . 'module';
		$lsPathConfig = $lsPathProject . DS . 'config';
		
		if (file_exists($lsPathModules))
		{
			$lsPathModule = $lsPathModules . DS . $this->sModuleName;
			$this->createDir($lsPathModule);
			
			if (file_exists($lsPathModule))
			{
				$lsFileLicense = $this->getLicense($this->sModuleName);
				
				$this->saveFile($lsPathModule, $this->bIsConsole ? 'console-Module' : 'Module', $lsFileLicense);
				$this->saveFile($lsPathModule, '#composer', '', 'json');
				
				$lsPathModuleConfig = $lsPathModule . DS . 'config';
				$this->createDir($lsPathModuleConfig);	

				if ($this->bIsRestful)
				{
					$this->saveFile($lsPathModuleConfig, 'restful-module.config', $lsFileLicense);
				}
				elseif ($this->bIsConsole)
				{
					$this->saveFile($lsPathModuleConfig, 'console-module.config', $lsFileLicense);
				}				
				elseif ($this->sModuleName == "Backend")
				{
				    $this->saveFile($lsPathModuleConfig, 'dashboard.config', $lsFileLicense);
				}
				elseif ($this->sModuleName == "Frontend")
				{
				    $this->saveFile($lsPathModuleConfig, 'site.config', $lsFileLicense);
				}
				else
				{
				    $this->saveFile($lsPathModuleConfig, 'module.config', $lsFileLicense);
				}
				
				$lsModuleRoute = Str::slugfy($this->sModuleName);
				
				$lsPathView = $lsPathModule . DS . 'view';
				$this->createDir($lsPathView);
				$lsPathViewModule = $lsPathView . DS . $lsModuleRoute;
				$this->createDir($lsPathViewModule);

				if (!$this->bIsRestful && !$this->bIsConsole)
				{
					$lsPathViewController = $lsPathViewModule . DS . $lsModuleRoute;
					$this->createDir($lsPathViewController);
					
					if ($this->sModuleName == "Backend")
					{
						$this->saveFile($lsPathViewController, 'dashboard', $lsFileLicense);
					}
					elseif ($this->sModuleName == "Frontend")
					{
						$this->saveFile($lsPathViewController, 'site', $lsFileLicense);
					}
					else
					{
						$this->saveFile($lsPathViewController, 'actionIndex', $lsFileLicense);
						$this->saveFile($lsPathViewController, 'add', $lsFileLicense, 'phtml');
						$this->saveFile($lsPathViewController, 'edit', $lsFileLicense, 'phtml');
						$this->saveFile($lsPathViewController, 'duplicate', $lsFileLicense, 'phtml');
						$this->saveFile($lsPathViewController, 'trash', $lsFileLicense, 'phtml');
						$this->saveFile($lsPathViewController, 'view', $lsFileLicense, 'phtml');
					}
				}

				$lsPathSrc = $lsPathModule . DS . 'src';
				$this->createDir($lsPathSrc);
										
				if (file_exists($lsPathSrc))
				{					
					$lsPathController = $lsPathSrc . DS . 'Controller';
					$this->createDir($lsPathController);
					
					if ($this->bIsConsole)
					{
						$this->saveFile($lsPathController, 'console-_Controller', $lsFileLicense, 'php', $this->sModuleName, true);
						$lsPathController .= DS . $this->sModuleName . 'ConsoleController.php';
						$this->addMethodToController($lsPathController, $this->sModuleName, $this->sModuleName, $lsModuleRoute);
					}
			        elseif ($this->bIsRestful)
			        {
						$this->saveFile($lsPathController, '_Controller', $lsFileLicense, 'php', $this->sModuleName, true);
						
						$lsPathController .= DS . $this->sModuleName . 'RestfulController.php';
						$this->addMethodToController($lsPathController, $this->sModuleName, 'get', $lsModuleRoute, 'get');
						$this->addMethodToController($lsPathController, $this->sModuleName, 'post', $lsModuleRoute, 'post');
						$this->addMethodToController($lsPathController, $this->sModuleName, 'put', $lsModuleRoute, 'put');
						$this->addMethodToController($lsPathController, $this->sModuleName, 'delete', $lsModuleRoute, 'delete');
			        }
					elseif ($this->sModuleName == "Backend")
			        {
			            $this->saveFile($lsPathController, '_Dashboard', $lsFileLicense);
			        }
			        elseif ($this->sModuleName == "Frontend")
			        {
			            $this->saveFile($lsPathController, '_Site', $lsFileLicense);
			        }
			        else
			        {
						$this->saveFile($lsPathController, '_Controller', $lsFileLicense, 'php', $this->sModuleName);
			        }
			        
			        $lsPathFactory = $lsPathSrc . DS . 'Factory';
					$this->createDir($lsPathFactory);
					$lsControlerFactoryName = $this->bIsConsole ? 'console-_ControllerFactory' : '_ControllerFactory';
			        $this->saveFile($lsPathFactory, $lsControlerFactoryName, $lsFileLicense, 'php', $this->sModuleName, true);

			        $lsPathService = $lsPathSrc . DS . 'Service';
					$this->createDir($lsPathService);
					$lsServiceName = '_Service';
					$lsServiceName = $this->bIsConsole ? 'console-_Service' : $lsServiceName;
					$lsServiceName = $this->bIsRestful ? 'restful-_Service' : $lsServiceName;
					$this->saveFile($lsPathService, $lsServiceName, $lsFileLicense, 'php', $this->sModuleName, true);
					$lsServiceFactoryName = $this->bIsConsole ? 'console-_ServiceFactory' : '_ServiceFactory';
					$this->saveFile($lsPathFactory, $lsServiceFactoryName, $lsFileLicense, 'php', $this->sModuleName, true);
					
					if (!$this->bIsRestful && !$this->bIsConsole)
					{
						$lsPathView = $lsPathSrc . DS . 'View';
						$this->createDir($lsPathView);
						$lsPathViewHelper = $lsPathView . DS . 'Helper';
						$this->createDir($lsPathViewHelper);
						
						$lsPathPlugin = $lsPathSrc . DS . 'Plugin';
						$this->createDir($lsPathPlugin);
					}

			        if ($this->sModuleName != "Backend" && $this->sModuleName != "Frontend")
					{
						$lsPathEntity = $lsPathSrc . DS . 'Entity';
    					$this->createDir($lsPathEntity);
    					$this->saveFile($lsPathEntity, 'Entity', $lsFileLicense);
    					$this->saveFile($lsPathEntity, '_Basic', $lsFileLicense);
    					$this->saveFile($lsPathEntity, '_Extended', $lsFileLicense);

    					$lsPathRepository = $lsPathSrc . DS . 'Repository';
    					$this->createDir($lsPathRepository);
    					$this->saveFile($lsPathRepository, '_Repository', $lsFileLicense);
						
						if (!$this->bIsRestful && !$this->bIsConsole)
						{
							$lsPathForm = $lsPathSrc . DS . 'Form';
							$this->createDir($lsPathForm);
							$this->saveFile($lsPathForm, '_Form', $lsFileLicense);
							
							$lsPathGrid = $lsPathSrc . DS . 'Grid';
							$this->createDir($lsPathGrid);
							$this->saveFile($lsPathGrid, '_Grid', $lsFileLicense);

							$this->setMenu($this->sModuleName);
						}

						$lsPathValidator = $lsPathSrc . DS . 'Validator';
						$this->createDir($lsPathValidator);
						
						$this->setModuleAutoload($lsPathConfig);
					}
				}
				
				$lsPathTest = $lsPathModule . DS . 'test';
				$this->createDir($lsPathTest);
				
				$lsPathLanguage = $lsPathModule . DS . 'language';
				$this->createDir($lsPathLanguage);
			}
		}
		else
		{
			Prompt::echoError("Project folder do not exist! You need to create a new project first. Please, use --help for further information.");
		}
	}
	
	
	/**
	 * 
	 */
	public function newController () : void
	{
		$lsPathModule = $this->getModulePath();
		
		$lsControllerName = ucfirst($this->getRequestArg('controller', null, true));
		
		if ($lsControllerName == null)
		{
			Prompt::echoError("The param controller is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module.config.php';
				
		$lsModuleRoute = Str::slugfy($this->sModuleName);
		$lsControllerRoute = Str::slugfy($lsControllerName);
				
		if (file_exists($lsPathModuleConfig))
		{
			$lsFileContent = System::localRequest($lsPathModuleConfig);

			if ($this->bIsConsole)
			{
				$lsSufix = $this->sConsoleSufix;
				$lsModuleController = "/*moduleController*/";
			}
			elseif ($this->bIsRestful)
			{
				$lsSufix = $this->sRestfulSufix;
				$lsModuleController = TemplateService::controllerRestful($lsControllerName, $lsControllerRoute);
			}
			else
			{
				$lsSufix = "";
				$lsModuleController = TemplateService::controller($lsControllerName, $lsControllerRoute);
			}

			Util::parse($lsFileContent, "\/\*moduleController\*\/|\/\* moduleController \*\/", $lsModuleController);
			
			$lsNamespace = "Controller\\{$lsControllerName}{$lsSufix}Controller::class";
			$lsFactory = "Factory\\{$lsControllerName}{$lsSufix}ControllerFactory::class";
			Util::parse($lsFileContent, "\/\*ControlerFactories\*\/|\/\* ControlerFactories \*\/", "{$lsNamespace} => {$lsFactory},\n\t\t\t/*ControlerFactories*/");

			$lsNamespace = "Service\\{$lsControllerName}{$lsSufix}Service::class";
			$lsFactory = "Factory\\{$lsControllerName}{$lsSufix}ServiceFactory::class";
			Util::parse($lsFileContent, "\/\*ServiceManagerFactories\*\/|\/\* ServiceManagerFactories \*\/", "{$lsNamespace} => {$lsFactory},\n\t\t\t/*ServiceManagerFactories*/");

			if ($this->bIsConsole)
			{
				$lsRoute = TemplateService::routeConsole($this->sModuleName, $lsControllerName, $lsControllerName);
				Util::parse($lsFileContent, "\/\*consoleroutes\*\/|\/\* consoleroutes \*\/", $lsRoute);
			}
			elseif ($this->bIsRestful)
			{
				$lsRoute = TemplateService::routeRestful($lsControllerRoute, $this->sModuleName, $lsControllerName);
				Util::parse($lsFileContent, "\/\*routes\*\/|\/\* routes \*\/", $lsRoute);
			}
			else
			{
				$lsRoute = TemplateService::route($lsControllerRoute, $this->sModuleName, $lsControllerName);
				Util::parse($lsFileContent, "\/\*routes\*\/|\/\* routes \*\/", $lsRoute);
			}
			
			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
				
		if (!$this->bIsRestful && !$this->bIsConsole)
		{
			$lsPathView = $lsPathModule . DS . 'view';
			$lsPathViewModule = $lsPathView . DS . $lsModuleRoute;
			$lsPathViewController = $lsPathViewModule . DS . $lsControllerRoute;
			$this->createDir($lsPathViewController);
			
			$this->saveFile($lsPathViewController, 'actionIndex', $lsFileLicense);
			$this->saveFile($lsPathViewController, 'add', $lsFileLicense, 'phtml');
			$this->saveFile($lsPathViewController, 'edit', $lsFileLicense, 'phtml');
			$this->saveFile($lsPathViewController, 'duplicate', $lsFileLicense, 'phtml');
			$this->saveFile($lsPathViewController, 'trash', $lsFileLicense, 'phtml');
			$this->saveFile($lsPathViewController, 'view', $lsFileLicense, 'phtml');
		}

		$lsPathSrc = $lsPathModule . DS . 'src';
				
		if (file_exists($lsPathSrc))
		{
			$lsPathController = $lsPathSrc . DS . 'Controller';
			$lsControlerName = $this->bIsConsole ? 'console-_Controller' : '_Controller';
			$this->saveFile($lsPathController, $lsControlerName, $lsFileLicense, 'php', $lsControllerName, true);
			
			if ($this->bIsRestful)
			{
				$lsPathController .= DS . $lsControllerName . $lsSufix . 'Controller.php';
				$this->addMethodToController($lsPathController, $this->sModuleName, 'get', $lsModuleRoute, 'get');
				$this->addMethodToController($lsPathController, $this->sModuleName, 'post', $lsModuleRoute, 'post');
				$this->addMethodToController($lsPathController, $this->sModuleName, 'put', $lsModuleRoute, 'put');
				$this->addMethodToController($lsPathController, $this->sModuleName, 'delete', $lsModuleRoute, 'delete');
			}

			$lsPathFactory = $lsPathSrc . DS . 'Factory';
			$lsControlerFactoryName = $this->bIsConsole ? 'console-_ControllerFactory' : '_ControllerFactory';
			$this->saveFile($lsPathFactory, $lsControlerFactoryName, $lsFileLicense, 'php', $lsControllerName, true);
				
			$lsPathService = $lsPathSrc . DS . 'Service';
			$lsServiceName = '_Service';
			$lsServiceName = $this->bIsConsole ? 'console-_Service' : $lsServiceName;
			$lsServiceName = $this->bIsRestful ? 'restful-_Service' : $lsServiceName;
			$this->saveFile($lsPathService, $lsServiceName, $lsFileLicense, 'php', $lsControllerName, true);
			$lsServiceFactoryName = $this->bIsConsole ? 'console-_ServiceFactory' : '_ServiceFactory';
			$this->saveFile($lsPathFactory, $lsServiceFactoryName, $lsFileLicense, 'php', $lsControllerName, true);

			if (!$this->bIsRestful)
			{
				$this->setMenu($lsControllerName);
			}
		}
	}

	
	/**
	 * 
	 */
	public function newAction () : void
	{
	    $lsPathModule = $this->getModulePath();
	    
	    $lsControllerName = ucfirst($this->getRequestArg('controller', $this->sModuleName, true));
	    
	    if ($lsControllerName == null)
	    {
	        Prompt::echoError("The param controller is required! Please, use --help for further information.");
	        return;
	    }
		
		$lsSufix = "";

		if ($this->bIsRestful)
		{
			$lsSufix = $this->sRestfulSufix;
		}
		elseif ($this->bIsConsole)
		{
			$lsSufix = $this->sConsoleSufix;
		}

	    $lsPathSrc = $lsPathModule . DS . 'src';
	    $lsPathController = $lsPathSrc . DS . 'Controller' . DS . $lsControllerName . $lsSufix . 'Controller.php';
	    
	    if (!file_exists($lsPathController))
	    {
	        Prompt::exitError("The controller file do not exist! You need to create a new controller first. Please, use --help for further information.");
	        return;
	    }
		
		if ($this->bIsRestful)
		{
			$lsMethod = lcfirst($this->getRequestArg('method', null, true));
		}

	    $lsActionName = lcfirst($this->getRequestArg('action', null, true));
	    
	    if ($lsActionName == null)
	    {
	        Prompt::echoError("The param action is required! Please, use --help for further information.");
	        return;
	    }
	    
	    $lsActionTitle = ucfirst($this->getRequestArg('title', ucfirst($lsActionName), false));
	    
	    $lsFileLicense = $this->getLicense($this->sModuleName);
	    
	    $lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module.config.php';
	    
	    $lsModuleRoute = Str::slugfy($this->sModuleName);
	    $lsControllerRoute = Str::slugfy($lsControllerName);
	    $lsActionRoute = Str::slugfy($lsActionName);
		
		$lsFileContent = System::localRequest($lsPathModuleConfig);

	    if (file_exists($lsPathModuleConfig) && !$this->bIsConsole)
	    {
			$lsRoute = "/{$lsControllerRoute}/{$lsActionRoute}";

			if ($this->bIsRestful)
			{
				$lsRoute = "/api/{$lsControllerRoute}";
				$lsActionRoute = $lsMethod;
			}

			$lsControllerAction = TemplateService::action( 
				$lsActionRoute, 
				$lsActionTitle, 
				$lsRoute, 
				$lsControllerName, 
				$lsSufix
			);
	        
	        Util::parse($lsFileContent, "\/\*controllerAction{$lsControllerName}{$lsSufix}\*\/|\/\* controllerAction{$lsControllerName}{$lsSufix} \*\/", $lsControllerAction);
	        
	        if (System::saveFile($lsPathModuleConfig, $lsFileContent))
	        {
	            $this->changedFileMsg($lsPathModuleConfig);
	        }
	    }
		
		if (!$this->bIsRestful && !$this->bIsConsole)
		{
			$lsPathView = $lsPathModule . DS . 'view';
			$lsPathViewModule = $lsPathView . DS . $lsModuleRoute;
			$lsPathViewController = $lsPathViewModule . DS . $lsControllerRoute;
			
			$this->saveFile($lsPathViewController, 'action', $lsFileLicense, 'phtml', $lsActionRoute);
		}

		$this->addMethodToController($lsPathController, $lsControllerName, $lsActionName, $lsControllerRoute, $lsActionRoute);

		if ($this->bIsConsole)
		{
			$lsRoute = TemplateService::routeConsole($this->sModuleName, $lsControllerName, $lsActionName);
			Util::parse($lsFileContent, "\/\*consoleroutes\*\/|\/\* consoleroutes \*\/", $lsRoute);

			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
	}

	
	/**
	 * 
	 * @param string $psPathController
	 * @param string $psControllerName
	 * @param string $psActionName
	 * @param string $psRoute
	 * @param string $psMethod
	 */
	public function addMethodToController (string $psPathController, string $psControllerName, string $psActionName, string $psRoute = "", string $psMethod = "") : void
	{
	    $lsFileCtrlContent = System::localRequest($psPathController);
		
		if ($this->bIsRestful)
		{
			$lsMethodHandler = TemplateService::methodHandler($psActionName, $psMethod);

			Util::parse($lsFileCtrlContent, "\/\*addHttpMethodHandler\*\/|\/\* addHttpMethodHandler \*\/", $lsMethodHandler);
			
			$psMethod = ucfirst($psMethod);
			
			$lsActionMethod = TemplateService::actionMethodRestful($psActionName, $psMethod, $psRoute, $psControllerName);
		}
		elseif ($this->bIsConsole)
		{
			$lsActionMethod = TemplateService::actionMethodConsole($psActionName);
		}
		else
		{
			$lsActionMethod = TemplateService::actionMethod($psActionName);
		}

	    Util::parse($lsFileCtrlContent, "}\n$|}$", $lsActionMethod);
	    
	    if (System::saveFile($psPathController, $lsFileCtrlContent))
	    {
	        $this->changedFileMsg($psPathController);
	    }
	}
	

	/**
	 *
	 * 
	 */
	public function newService () : void
	{
		$lsPathModule = $this->getModulePath();
		
		$lsServiceName = ucfirst($this->getRequestArg('service', null, true));
		
		if ($lsServiceName == null)
		{
			Prompt::echoError("The param service is required! Please, use --help for further information.");
			return;
		}

		$lsSufix = "";
		
		if ($this->bIsRestful)
		{
			$lsSufix = $this->sRestfulSufix;
		}
		elseif ($this->bIsConsole)
		{
			$lsSufix = $this->sConsoleSufix;
		}

		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module.config.php';
				
		if (file_exists($lsPathModuleConfig))
		{
			$lsFileContent = System::localRequest($lsPathModuleConfig);

			$lsNamespace = "Service\\{$lsServiceName}{$lsSufix}Service::class";
			$lsFactory = "Factory\\{$lsServiceName}{$lsSufix}ServiceFactory::class";
			Util::parse($lsFileContent, "\/\*ServiceManagerFactories\*\/|\/\* ServiceManagerFactories \*\/", "{$lsNamespace} => {$lsFactory},\n\t\t\t/*ServiceManagerFactories*/");

			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
				
		$lsPathSrc = $lsPathModule . DS . 'src';
				
		if (file_exists($lsPathSrc))
		{
			$lsPathService = $lsPathSrc . DS . 'Service';
			$this->saveFile($lsPathService, "_Service", $lsFileLicense, 'php', $lsServiceName, true);
			$lsPathFactory = $lsPathSrc . DS . 'Factory';
			$this->saveFile($lsPathFactory, "_ServiceFactory", $lsFileLicense, 'php', $lsServiceName, true);
		}
	}


	/**
	 *
	 * 
	 */
	public function newHelper () : void
	{
		$lsPathModule = $this->getModulePath();
		
		$lsHelperName = ucfirst($this->getRequestArg('helper', null, true));
		
		if ($lsHelperName == null)
		{
			Prompt::echoError("The param helper is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module.config.php';
				
		if (file_exists($lsPathModuleConfig))
		{
			$lsFileContent = System::localRequest($lsPathModuleConfig);

			$lsNamespace = "View\Helper\\{$lsHelperName}Helper::class";
			$lsFactory = "InvokableFactory::class";
			Util::parse($lsFileContent, "\/\*ViewHelperFactories\*\/|\/\* ViewHelperFactories \*\/", "{$lsNamespace} => {$lsFactory},\n\t\t\t/*ViewHelperFactories*/");
			Util::parse($lsFileContent, "\/\*ViewHelperAliases\*\/|\/\* ViewHelperAliases \*\/", "'{$lsHelperName}' => {$lsNamespace},\n\t\t\t/*ViewHelperAliases*/");

			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
				
		$lsPathSrc = $lsPathModule . DS . 'src';
				
		if (file_exists($lsPathSrc))
		{
			$lsPathHelper = $lsPathSrc . DS . 'View' . DS . 'Helper';
			$this->saveFile($lsPathHelper, '_Helper', $lsFileLicense, 'php', $lsHelperName);
		}
	}


	/**
	 *
	 *
	 */
	public function newPlugin () : void
	{
		$lsPathModule = $this->getModulePath();

		$lsPluginName = ucfirst($this->getRequestArg('plugin', null, true));
		
		if ($lsPluginName == null)
		{
			Prompt::echoError("The param plugin is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathModuleConfig = $lsPathModule . DS . 'config' . DS . 'module.config.php';
				
		if (file_exists($lsPathModuleConfig))
		{
			$lsFileContent = System::localRequest($lsPathModuleConfig);

			$lsNamespace = "Plugin\\{$lsPluginName}Plugin::class";
			$lsFactory = "InvokableFactory::class";
			Util::parse($lsFileContent, "\/\*ControllerPluginsFactories\*\/|\/\* ControllerPluginsFactories \*\/", "{$lsNamespace} => {$lsFactory},\n\t\t\t/*ControllerPluginsFactories*/");
			Util::parse($lsFileContent, "\/\*ControllerPluginsAliases\*\/|\/\* ControllerPluginsAliases \*\/", "'{$lsPluginName}' => {$lsNamespace},\n\t\t\t/*ControllerPluginsAliases*/");

			if (System::saveFile($lsPathModuleConfig, $lsFileContent))
			{
			    $this->changedFileMsg($lsPathModuleConfig);
			}
		}
				
		$lsPathSrc = $lsPathModule . DS . 'src';
		
		if (file_exists($lsPathSrc))
		{
			$lsPathPlugin = $lsPathSrc . DS . 'Plugin';
			$this->saveFile($lsPathPlugin, '_Plugin', $lsFileLicense, 'php', $lsPluginName);
		}
	}


	/**
	 *
	 *
	 */
	public function newValidator () : void
	{
		$lsPathModule = $this->getModulePath();

		$lsValidatorinName = ucfirst($this->getRequestArg('validator', null, true));
		
		if ($lsValidatorinName == null)
		{
			Prompt::echoError("The param validator is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathSrc = $lsPathModule . DS . 'src';
		
		if (file_exists($lsPathSrc))
		{
			$lsPathValidator = $lsPathSrc . DS . 'Validator';
			$this->saveFile($lsPathValidator, '_Validator', $lsFileLicense, 'php', $lsValidatorinName);
		}
	}


	/**
	 *
	 *
	 */
	public function newFactory () : void
	{
		$lsPathModule = $this->getModulePath();

		$lsFactoryName = ucfirst($this->getRequestArg('factory', null, true));
		
		if ($lsFactoryName == null)
		{
			Prompt::echoError("The param factory is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
				
		$lsPathSrc = $lsPathModule . DS . 'src';
		
		if (file_exists($lsPathSrc))
		{
			$lsPathFactory = $lsPathSrc . DS . 'Factory';
			$this->saveFile($lsPathFactory, '_Factory', $lsFileLicense, 'php', $lsFactoryName);
		}
	}
	
	
	/**
	 *
	 *
	 */
	public function newTest () : void
	{
		$lsPathModule = $this->getModulePath();
		
		$lsTestType = ucfirst($this->getRequestArg('testtype', 'Service', true));
		$lsTestName = ucfirst($this->getRequestArg('testname', null, true));

		if ($lsTestType== null)
		{
			Prompt::echoError("The param typeTest is required! Please, use --help for further information.");
			return;
		}
		
		if ($lsTestName == null)
		{
			Prompt::echoError("The param testName is required! Please, use --help for further information.");
			return;
		}
		
		$lsFileLicense = $this->getLicense($this->sModuleName);
		
		$lsPathTest = $lsPathModule . DS . 'test';
		
		if (file_exists($lsPathTest))
		{
			$lsPathTestType = $lsPathTest . DS . $lsTestType;
			
			if (!file_exists($lsPathTestType))
			{
				$this->createDir($lsPathTestType);
			}
			
			$this->saveFile($lsPathTestType, '_Test', $lsFileLicense, 'php', $lsTestName.$lsTestType);
		}
	}

	
	/**
	 * 
	 */
	public function setForm () : void
	{
		$lsPathModule = $this->getModulePath();

		$lsFormName = ucfirst($this->getRequestArg('form', $this->sModuleName, false));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);

		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,		        
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		    'database' => $lsDbName,
		];
		
		$this->aRepository['Db'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['Db']->connect())
		{
		    $lsTableName = $this->getRequestArg('table', $this->sModuleName, true);
		    
		    $laTable = $this->aRepository['Db']->descEntity($lsTableName);

			if (is_array($laTable))
			{
			    $lsField = "";
                
			    foreach ($laTable as $laField)
			    {
					switch ($laField['Field'])
					{
						case 'id':
						case 'stHash':
						case 'SysAccount_id':
						case 'SysUserOwner_id':
						case 'txtTag':
						case 'dtInsert':
						case 'dtUpdate':
						case 'numStatus':
						case 'isActive':
							break;
						default:
							$lsRequired = 'false';
							
							if ($laField['Null'] == 'NO')
							{
								$lsRequired = 'true';
							}
							
							$lsField .= TemplateService::formField($laField['Field'], $lsRequired);
			        }
			    }
			    
		        $lsPathSrc = $lsPathModule . DS . 'src';			    
		        $lsPathForm = $lsPathSrc . DS . 'Form';
    		    
    		    if (!file_exists($lsPathForm))
    		    {
    		    	$this->createDir($lsPathForm);
    		    }
    		    
    		    $lsPathFormFile = $lsPathForm . DS . "{$lsFormName}Form.php";
    		    
    		    if (!file_exists($lsPathFormFile))
    		    {
    		    	$lsFileLicense = $this->getLicense($this->sModuleName);
    		    	$this->saveFile($lsPathForm, '_Form', $lsFileLicense, 'php', $lsFormName);
    		    }
    		    
    		    $lsFileContent = System::localRequest($lsPathFormFile);
			
		        Util::parse($lsFileContent, "#%FIELDS%#", $lsField);
		        
		        if (System::saveFile($lsPathFormFile, $lsFileContent))
		        {
		            $this->changedFileMsg($lsPathFormFile);
		        }
			}
			else 
			{
			    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
			}
		}
		else 
		{
		    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
		}
	}
	
	
	/**
	 *
	 */
	public function setGrid () : void
	{
		$lsPathModule = $this->getModulePath();

		$lsGridName = ucfirst($this->getRequestArg('grid', $this->sModuleName, false));
		
		$lsIsSearchWindow = $this->getRequestArg('searchwindow', 'n', false);
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);

		$laDbConf = [
				'driver' => $lsDbDriver,
				'charset' => $lsDbCharset,
				'hostname' => $lsDbHost,
				'port' => $lsDbPort,
				'username' => $lsDbUser,
				'password' => $lsDbPass,
				'database' => $lsDbName,
		];
		
		$this->aRepository['Db'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['Db']->connect())
		{
			$lsTableName = $this->getRequestArg('table', $this->sModuleName, true);
			
			$laTable = $this->aRepository['Db']->descEntity($lsTableName);
			
			if (is_array($laTable))
			{
				$lsCol = "";
				
				foreach ($laTable as $laField)
				{
					switch ($laField['Field'])
					{
						case 'id':
						case 'stHash':
						case 'SysAccount_id':
						case 'SysUserOwner_id':
						case 'txtTag':
						case 'dtInsert':
						case 'dtUpdate':
						case 'numStatus':
						case 'isActive':
							break;
						default:
							$lsCol .= TemplateService::gridCol($laField['Field']);
					}
				}
				
				$lsPathSrc = $lsPathModule . DS . 'src';
				$lsPathGrid = $lsPathSrc . DS . 'Grid';
				
				if (!file_exists($lsPathGrid))
				{
					$this->createDir($lsPathGrid);
				}

				if (strtolower($lsIsSearchWindow) == 's')
				{
					$lsPathGridFile = $lsPathGrid . DS . "{$lsGridName}SearchGrid.php";
				}
				else
				{
					$lsPathGridFile = $lsPathGrid . DS . "{$lsGridName}Grid.php";
				}
				
				if (!file_exists($lsPathGridFile))
				{
					$lsFileLicense = $this->getLicense($this->sModuleName);

					if (strtolower($lsIsSearchWindow) == 's')
					{
						$this->saveFile($lsPathGrid, '_SearchGrid', $lsFileLicense, 'php', $lsGridName);
					}
					else 
					{
						$this->saveFile($lsPathGrid, '_Grid', $lsFileLicense, 'php', $lsGridName);
					}
				}
				
				$lsFileContent = System::localRequest($lsPathGridFile);
				
				Util::parse($lsFileContent, "#%COLUMS%#", $lsCol);
				
				if (System::saveFile($lsPathGridFile, $lsFileContent))
				{
				    $this->changedFileMsg($lsPathGridFile);
				}
			}
			else
			{
				Prompt::echoError($this->aRepository['Db']->getErrorMsg());
			}
		}
		else
		{
			Prompt::echoError($this->aRepository['Db']->getErrorMsg());
		}
	}
	
	
	/**
	 * 
	 */
	public function setEntity () : void
	{
		$lsPathModule = $this->getModulePath();
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);
		
		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,		        
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		    'database' => $lsDbName,
		];
		
		$this->aRepository['Db'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['Db']->connect())
		{
		    $lsTableName = $this->getRequestArg('table', $this->sModuleName, true);
		    
		    $laTable = $this->aRepository['Db']->descEntity($lsTableName);

			if (is_array($laTable))
			{
			    $lsField = "";
			    
			    foreach ($laTable as $laField)
			    {
			        $lsF = $laField['Field'];
			        
			        if ($lsF != 'id' && $lsF != 'stHash' && $lsF != 'SysAccount_id' && $lsF != 'SysUserOwner_id' && $lsF != 'txtTag' && $lsF != 'dtInsert' && $lsF != 'dtUpdate' && $lsF != 'numStatus' && $lsF != 'isActive')
			        {
    			        $lsDefault = "";
    			        $lsDefaultType = 'string';
    			        $lsPri = "";
    			        $lsAuto = "";
    			        $lsNull = ' * @ORM\Column(nullable=true)';
    			        
    			        if ($laField['Key'] == 'PRI')
    			        {
    			            $lsPri = "\t" . ' * @ORM\Id' . "\n";
    			        }
    			        
    			    	if ($laField['Extra'] == 'auto_increment')
    			        {
    			            $lsAuto = "\t" . ' * @ORM\GeneratedValue(strategy="AUTO")' . "\n";
    			        }
    			        
    			        if ($laField['Null'] == 'NO')
    			        {
    			            $lsNull = ' * @ORM\Column(nullable=false)';
    			        }
    			        
    			        $laType = explode("(", $laField['Type']);
    			        $lsType = $laType[0];
    			        
    			    	switch ($lsType)
    			        {
    			            case 'int':
    			            case 'tinyint':
    			            case 'smallint':
    			            case 'mediumint':
    			            case 'bigint':
								$lsFieldType = ' * @var int';
								$lsFieldTypeORM = ' * @ORM\Column(type="integer")';
    			                $lsDefaultType = 'int';
    			                break;
    			            case 'text':
    			            case 'tinytext':
    			            case 'mediumtext':
    			            case 'longtext':
    			            case 'blob':
    			            case 'tinyblob':
    			            case 'mediumblob':
							case 'longblob':
								$lsFieldType = ' * @var string';
								$lsFieldTypeORM = ' * @ORM\Column(type="text")';
    			                break;
    			            case 'date':
    			            case 'time':			                 
							case 'datetime':
								$lsFieldType = ' * @var string';
								$lsFieldTypeORM = ' * @ORM\Column(type="datetime")';
								break;
							case 'timestamp':
								$lsFieldType = ' * @var int';
								$lsFieldTypeORM = ' * @ORM\Column(type="integer")';
								$lsDefaultType = 'int';
								break;
    			            case 'decimal':
    			            case 'float':
    			            case 'double':
							case 'real':
								$lsFieldType = ' * @var float';
								$lsFieldTypeORM = ' * @ORM\Column(type="decimal")';
								$lsDefaultType = 'decimal';
    			                break;
							case 'boolean':
								$lsFieldType = ' * @var bool';
    			                $lsFieldTypeORM = ' * @ORM\Column(type="boolean")';
    			                break;
							default:
								$lsFieldType = ' * @var string';
								$lsFieldTypeORM = ' * @ORM\Column(type="string")';
    			        }	
    			        
			            if ($laField['Default'] == '0' || !empty($laField['Default']))
    			        {   
    			            if ($lsDefaultType == 'string')
    			            {
    			                $lsDefault = " = '{$laField['Default']}'";
    			            }
    			            else
    			            {
    			                $lsDefault = " = {$laField['Default']}";
    			            }
    			        }    			        
    			        
    			        $lsField .= "/**\n{$lsPri}{$lsAuto}\t{$lsFieldTypeORM}\n\t{$lsNull}\n\t{$lsFieldType}\n\t */\n\tprotected \${$laField['Field']}{$lsDefault};\n\n\t";
			        }
			    }
			    
			    $lsPathSrc = $lsPathModule . DS . 'src';			    
			    $lsPathEntity = $lsPathSrc . DS . 'Entity';
    		    
    		    if (!file_exists($lsPathEntity))
    		    {
    		    	$this->createDir($lsPathEntity);
    		    }
    		    
    		    $lsPathEntityFile = $lsPathEntity . DS . "{$lsTableName}.php";
    		    
    		    if (!file_exists($lsPathEntityFile))
    		    {
    		    	$lsFileLicense = $this->getLicense($this->sModuleName);
					$this->saveFile($lsPathEntity, 'Entity', $lsFileLicense, 'php', $lsTableName);
					$this->saveFile($lsPathEntity, '_Basic', $lsFileLicense, 'php', $lsTableName);
					$this->saveFile($lsPathEntity, '_Extended', $lsFileLicense, 'php', $lsTableName);
    		    }

				$lsPathRepository = $lsPathSrc . DS . 'Repository';
				$lsPathRepositoryFile = $lsPathRepository . DS . "{$lsTableName}Repository.php";

				if (!file_exists($lsPathRepositoryFile))
				{
					$this->createDir($lsPathRepository);
					$this->saveFile($lsPathRepository, '_Repository', $lsFileLicense, 'php', $lsTableName);
				}
    		    
    		    $lsFileContent = System::localRequest($lsPathEntityFile);
                
		        Util::parse($lsFileContent, "#%FIELDS%#", $lsField);
		        
		        if (System::saveFile($lsPathEntityFile, $lsFileContent))
		        {
		            $this->changedFileMsg($lsPathEntityFile);
		        }
			}
			else 
			{
			    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
			}
		}
		else 
		{
		    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
		}
	}


	/**
	 * 
	 */
	public function layoutGit () : void
	{
		$lsDependency = System::localRequest(BASE_DIR . DS . "layout" . DS . "layout.json");
		$laDependency = json_decode($lsDependency, true);
		Debug::debug($laDependency);
		
		if (isset($laDependency['require']) && is_array($laDependency['require']))
		{
			System::createDir($this->sLayoutVendor);
			
			foreach ($laDependency['require'] as $lsPackage => $lsRelease)
			{
				$laPackage = explode("/", $lsPackage);
				$lsPackageDir = $this->sLayoutVendor . DS . $laPackage[0];
				$lsPackageReleaseDir = $lsPackageDir . DS . $laPackage[1] . "-" . preg_replace("/[^0-9\.]/", "", $lsRelease);
				System::createDir($lsPackageDir);
				
				if (is_dir($lsPackageDir) && !is_dir($lsPackageReleaseDir))
				{
					chdir($lsPackageDir);
					
					echo("Getting https://github.com/{$lsPackage}/archive/{$lsRelease}.tar.gz\n");
					System::execute("wget https://github.com/{$lsPackage}/archive/{$lsRelease}.tar.gz");
					
					$lsGzFile = $lsRelease . ".tar.gz";
					$lsGzFilePath = $lsPackageDir . DS . $lsGzFile;
					
					if (is_file($lsGzFilePath))
					{
						System::execute("tar -xzf {$lsGzFile}");
						System::removeFile($lsGzFile);

						$loDir = dir($lsPackageDir);

						if (false != $loDir)
						{
							while (false !== ($lsRes = $loDir->read()))
							{
								if (is_link($lsRes)) continue;

								$lsResLower = strtolower($lsRes);
								System::execute("mv {$lsRes} {$lsResLower}");

								if (preg_match("/$laPackage[1]/i", $lsResLower))
								{
									if (is_dir($lsResLower))
									{
										$lsLink = $laPackage[1];
										
										if (is_link($lsLink))
										{
											System::removeSimblink($lsLink);
										}

										System::simblink($lsResLower, $lsLink);
										$this->clearDir($lsPackageDir . DS . $lsResLower);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * 
	 * @param string $psPackageReleaseDir
	 */
	public function clearDir (string $psPackageReleaseDir) : void
	{
		chdir($psPackageReleaseDir);
		
		if (is_dir($psPackageReleaseDir . DS . "dist"))
		{
			$loDir = dir($psPackageReleaseDir);
				
			if (false != $loDir)
			{
				while (false !== ($lsResource = $loDir->read()))
				{
					Debug::debug($lsResource);
			
					if ($lsResource != '.' && 
						$lsResource != '..'	&& 
						$lsResource != 'dist' && 
						$lsResource != 'lang' && 
						$lsResource != 'locale' && 
						$lsResource != 'min')
					{
						if (is_dir($loDir->path . DS . $lsResource))
						{
							System::removeDir($loDir->path . DS . $lsResource);
						}
						else if (is_file($loDir->path . DS . $lsResource))
						{
							System::removeFile($loDir->path . DS . $lsResource);
						}
					}
				}
					
				$loDir->close();
			}
		}
		else
		{
			$loDir = dir($psPackageReleaseDir);
		
			$lsDependency = System::localRequest(BASE_DIR . DS . "layout" . DS . "layout.json");
			$laDependency = json_decode($lsDependency, true);
			Debug::debug($laDependency);
			
			if (isset($laDependency['ignore']))
			{
				$laPatern = $laDependency['ignore'];
			}
			else 
			{	
				$laPatern = [
					"^dev",
					"^src",
					"^build",
					"^external",
					"^test",
					"^log",
					"^error",
					"^task",
					"^extra",
					"^script",
					"^meteor",
					"^template",
					"^sample",
					"^samples",
					"^example",
					"^examples",
					"^demo",
					"^doc",
					"^scss",
					"^less",
					"^changelog",
					"^update",
					"\.json$",
					"\.md$",
					"\.txt$",
					"\.rb$",
					"\.yml$",
					"\.xml$",
					"\.html$",
					"\.htm$",
					"\.sh$",
					"gruntfile.js",
					"AUTHORS",
					"LICENCE",
					"^\."
				];
			}
					
			if (false != $loDir)
			{
				while (false !== ($lsResource = $loDir->read()))
				{
					Debug::debug($lsResource);
						
					if ($lsResource != '.' && 
						$lsResource != '..' && 
						$lsResource != 'dist' && 
						$lsResource != 'lang' && 
						$lsResource != 'locale' && 
						$lsResource != 'min')
					{
						foreach ($laPatern as $lsPatern)
						{
							if (preg_match("/$lsPatern/i", $lsResource))
							{
								if (is_dir($loDir->path . DS . $lsResource))
								{
									System::removeDir($loDir->path . DS . $lsResource);
								}
								else if (is_file($loDir->path . DS . $lsResource))
								{
									System::removeFile($loDir->path . DS . $lsResource);
								}
							}
						}
					}
				}
		
				$loDir->close();
			}
		}		
	}
	
	
	/**
	 *
	 */
	public function layoutDist () : void
	{
		$lsDependency = System::localRequest(BASE_DIR . DS . "layout" . DS . "layout.json");
		$laDependency = json_decode($lsDependency, true);
		Debug::debug($laDependency);
	
		if (isset($laDependency['dist']) && is_array($laDependency['dist']))
		{
			System::createDir($this->sLayoutVendor);
				
			foreach ($laDependency['dist'] as $lsPackage => $lsDist)
			{
				$laPackage = explode("/", $lsPackage);
				$lsPackageDir = $this->sLayoutVendor . DS . $laPackage[0];
				System::createDir($lsPackageDir);
	
				if (is_dir($lsPackageDir))
				{
					chdir($lsPackageDir);
						
					echo("Getting {$lsDist}\n");
					System::execute("wget {$lsDist}");

					$laDist = parse_url($lsDist);
					$laPath = explode("/", $laDist['path']);
						
					$lsGzFile = $laPath[count($laPath) - 1];
					$lsGzFilePath = $lsPackageDir . DS . $lsGzFile;
						
					if (is_file($lsGzFilePath))
					{
						if (preg_match("/.zip$/", $lsGzFile))
						{
							System::execute("unzip -o {$lsGzFile}");
						}
						elseif (preg_match("/.tar.gz$/", $lsGzFile))
						{
							System::execute("tar -xzf {$lsGzFile}");
						}
						
						System::removeFile($lsGzFile);

						$loDir = dir($lsPackageDir);

						if (false != $loDir)
						{
							while (false !== ($lsRes = $loDir->read()))
							{
								if (is_link($lsRes)) continue;
								
								$lsResLower = strtolower($lsRes);
								System::execute("mv {$lsRes} {$lsResLower}");

								if (preg_match("/$laPackage[1]/i", $lsResLower))
								{
									if (is_dir($lsResLower))
									{
										$lsLink = preg_replace("/-([0-9\.]+)/", "$1", $laPackage[1]);
										
										if (is_link($lsLink))
										{
											System::removeSimblink($lsLink);
										}

										System::simblink($lsResLower, $lsLink);
										$this->clearDir($lsPackageDir . DS . $lsResLower);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	/**
	 *
	 */
	public function installDb () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', null, true));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);

		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		];
		
		$this->aRepository['newDb'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['newDb']->connect())
		{
			if (!$this->aRepository['newDb']->checkDb($lsDbName))
			{
				if (Prompt::confirm("The database [{$lsDbName}] does not exists! Create a new database using dbname [{$lsDbName}]?", 'y'))
				{
					if ($this->aRepository['newDb']->createDb($lsDbName))
					{
						$laDbConf['database'] = $lsDbName;
					}
					else
					{
						Prompt::echoError($this->aRepository['newDb']->getErrorMsg());
						return;
					}
				}
				else 
				{
					Prompt::echoWarning("You need a database to continue. Aborting proccess!");
					return;
				}
			}
			elseif (Prompt::confirm("The database [{$lsDbName}] already exists! Confirm use this database?", 'n'))
			{
				$laDbConf['database'] = $lsDbName;
			}
			else
			{
				Prompt::echoWarning("You need a database to continue. Aborting proccess!");
				return;
			}
		}
		else 
		{
			Prompt::echoError($this->aRepository['newDb']->getErrorMsg());
			return;
		}

		$laDBTemplates = [
			1 => "onion-base.sql",
			#2 => "onion-full.sql",
			#3 => "onion-parking.sql",
			#4 => "onion-service.sql",
			#5 => "onion-telmsg.sql",
		];
		
		echo("Select a database model to install:\n");

		foreach ($laDBTemplates as $lsOpt => $lsLabel)
		{
			echo("[{$lsOpt}] {$lsLabel}\n");
		}

		++$lsOpt;
		echo("[{$lsOpt}] neither (default)\n");
		
		$lsAnswer = Prompt::prompt("Option:", "{$lsOpt}");
		
		$lsDbFile = isset($laDBTemplates[$lsAnswer]) ? $laDBTemplates[$lsAnswer] : null;
		
		if (is_null($lsDbFile))
		{
			$lsFile = $this->getRequestArg('dbtemplate');

			$lsDbFile = isset($laDBTemplates[$lsFile]) ? $this->sDbPath . DS . $laDBTemplates[$lsFile] : $lsFile;
		}
		else
		{
			$lsDbFile = $this->sDbPath . DS . $lsDbFile;
		}

		if (!is_null($lsDbFile))
		{
			$lsDbBase = System::localRequest($lsDbFile);
			
			$this->aRepository['newDb']->setDbConf($laDbConf);
			
			if (!$this->aRepository['newDb']->importDb($lsDbBase))
			{
				Prompt::echoError($this->aRepository['newDb']->getErrorMsg());
			}
			else
			{
				Prompt::echoSuccess("Success in configure database!");				
			}	
		}

		$this->confDb($laDbConf);
	}
	
	
	/**
	 * 
	 */
	public function createTableBase () : void
	{
	    $this->setProjectFolder($this->getRequestArg('project', null, true));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);

		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,		        
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		    'database' => $lsDbName,
		];
		
		$this->aRepository['Db'] = new InstallRepository($laDbConf);
		
		if ($this->aRepository['Db']->connect())
		{
		    $lsTableName = $this->getRequestArg('table', null, true);
		    
			if ($this->aRepository['Db']->createTableBase($lsTableName))
			{
			    Prompt::echoSuccess('Successful on create table base!');
			}
			else 
			{
			    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
			}
		}
		else
		{
		    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
		}
	}

	
	/**
	 * 
	 */
	public function createUserDb () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', null, true));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		
		$laDbConf = [
		    'driver' => $lsDbDriver,
		    'charset' => $lsDbCharset,		        
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
		];

		$this->aRepository['Db'] = new InstallRepository($laDbConf);

		if ($this->aRepository['Db']->connect())
		{
			$lsNewUser = $this->getRequestArg('newuser', null, true);
			$lsNewPass = $this->getRequestArg('newpass', null, true);
			$lsHost = $this->getRequestArg('authhost', '%');
			$lsDb = $this->getRequestArg('authdb', '*');
			$lsTable = $this->getRequestArg('authtable', '*');	
				
			if ($this->aRepository['Db']->createUser($lsHost, $lsNewUser, $lsNewPass, $lsDb, $lsTable))
			{
			    Prompt::echoSuccess('Successful on create user!');
			}
			else 
			{
			    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
			}
		}
		else
		{
		    Prompt::echoError($this->aRepository['Db']->getErrorMsg());
		}
	}
	
	
	/**
	 * 
	 * @param array|null $paDbConf
	 */
	public function confDb (?array $paDbConf = null) : void
	{
		$this->setProjectFolder($this->getRequestArg('project', null, true));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		if ($paDbConf == null)
		{
			$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
			$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
			$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
			$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
			$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
			$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
			$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);
		
			$paDbConf = [
		        'driver' => $lsDbDriver,
		        'charset' => $lsDbCharset,	
				'hostname' => $lsDbHost,
				'port' => $lsDbPort,
				'username' => $lsDbUser,
				'password' => $lsDbPass,
				'database' => $lsDbName
			];
		}
				
		$laNewDbConf['production']['driver'] = $paDbConf['driver'];
		$laNewDbConf['production']['charset'] = $paDbConf['charset'];
		$laNewDbConf['production']['hostname'] = $paDbConf['hostname'];
		$laNewDbConf['production']['port'] = $paDbConf['port'];
		$laNewDbConf['production']['username'] = $paDbConf['username'];
		$laNewDbConf['production']['password'] = $paDbConf['password'];
		$laNewDbConf['production']['database'] = $paDbConf['database'];
		
		$laNewDbConf['development']['driver'] = $paDbConf['driver'];
		$laNewDbConf['development']['charset'] = $paDbConf['charset'];		
		$laNewDbConf['development']['hostname'] = $paDbConf['hostname'];
		$laNewDbConf['development']['port'] = $paDbConf['port'];
		$laNewDbConf['development']['username'] = $paDbConf['username'];
		$laNewDbConf['development']['password'] = $paDbConf['password'];
		$laNewDbConf['development']['database'] = $paDbConf['database'];
		
		$lsFileContent = ArrayObject::arrayToFile($laNewDbConf);

		$lsDbLocalPath = $this->sProjectPath . DS . 'config' . DS . 'db.local.php';

		if (System::saveFile($lsDbLocalPath, $lsFileContent))
		{
		    $this->changedFileMsg($lsDbLocalPath);
		}
	}


	/**
	 * 
	 */
	public function setAuthRoles () : void
	{
		$this->setProjectFolder($this->getRequestArg('project', null, true));
		
		$laProjectConf = $this->getProjectConf('db');
		$lsEnv = 'development';
		$laDbProjectConf = $laProjectConf[$lsEnv];

		$lsDbDriver = $this->getRequestArg('driver', $laDbProjectConf['driver']);
		$lsDbCharset = $this->getRequestArg('charset', $laDbProjectConf['charset']);		
		$lsDbHost = $this->getRequestArg('host', $laDbProjectConf['hostname']);
		$lsDbPort = $this->getRequestArg('port', $laDbProjectConf['port']);
		$lsDbUser = $this->getRequestArg('user', $laDbProjectConf['username']);
		$lsDbPass = $this->getRequestArg('pass', $laDbProjectConf['password']);
		$lsDbName = $this->getRequestArg('dbname', $laDbProjectConf['database'], true);

		$laDbConf = [
	        'driver' => $lsDbDriver,
	        'charset' => $lsDbCharset,	
			'hostname' => $lsDbHost,
			'port' => $lsDbPort,
			'username' => $lsDbUser,
			'password' => $lsDbPass,
			'database' => $lsDbName
		];

		$this->aRepository['Db'] = new InstallRepository($laDbConf);

		$lsSql = $this->aRepository['Db']->select('SysUserGroup g', "g.numStatus = 0 AND g.isActive = '1'", ["group" => "g.stName", "father" => "f.stName", "auth" => "g.txtGroupPermission"], "LEFT JOIN SysUserGroup f ON f.id = g.SysUserGroup_id");

		$laResult = $this->aRepository['Db']->queryExec($lsSql);

		if (is_array($laResult))
		{
			$laRoles = null;

			foreach ($laResult as $laGroup)
			{
				$laRoles[$laGroup['group']] = $laGroup['father'];
				$laAuth[$laGroup['group']] = $laGroup['auth'];
			}

			if (is_array($laRoles))
			{
				Debug::debug(ArrayObject::arrayToFile($laRoles));

				$lsFilePath = $this->sProjectPath . DS . 'config' . DS . 'roles.php';
				
				if (System::saveFile($lsFilePath, ArrayObject::arrayToFile($laRoles)))
				{
					echo ("\e[32m+ File created: " . $lsFilePath . "\e[0m\n");
				}
			}

			if (is_array($laAuth))
			{
				foreach ($laAuth as $lsGroup => $lsContent)
				{
					$laContent = [];
					$laContent[$lsGroup] = json_decode((string)$lsContent, true);
					Debug::debug(ArrayObject::arrayToFile($laContent));

					$lsFilePath = $this->sProjectPath . DS . 'config' . DS . 'acl' . DS . "{$lsGroup}.php";
					
					if (System::saveFile($lsFilePath, ArrayObject::arrayToFile($laContent)))
					{
						echo ("\e[32m+ File created: " . $lsFilePath . "\e[0m\n");
					}
				}
			}
		}
	}
}