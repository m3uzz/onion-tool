<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Service;
use OnionApi\Config;
use OnionApi\Help;
use OnionApi\Abstracts\AbstractService;
use OnionLib\ArrayObject;
use OnionLib\Debug;
use OnionLib\Prompt;
use OnionLib\Str;
use OnionLib\System;
use OnionLib\Util;
use OnionTool\Repository\InstallRepository;


abstract class ToolServiceAbstract extends AbstractService
{

	/**
	 * @var string
	 */
	protected $bIsRestful = false;

	/**
	 * @var string
	 */
	protected $bIsConsole = false;
	
	/**
	 * @var string
	 */
	protected $sRestfulSufix = 'Restful';
	
	/**
	 * @var string
	 */
    protected $sConsoleSufix = 'Console';	

	/**
	 * @var string
	 */
    protected $sProjectPath;
    
	/**
	 * @var string
	 */    
	protected $sTemplatePath;
	
	/**
	 * @var string
	 */	
	protected $sDbPath;
	
	/**
	 * @var string
	 */	
	protected $sLayoutVendor;
	
	/**
	 * @var string
	 */	
	protected $sProjectFolder;
	
	/**
	 * @var string
	 */	
	protected $sProjectName;
	
	/**
	 * @var string
	 */	
	protected $sProjectDomain;
	
	/**
	 * @var string
	 */	
	protected $sModuleName = null;

	/**
	 * @var string
	 */
	protected $sAuthor = null;

	/**
	 * @var string
	 */
	protected $sEmail = null;

	/**
	 * @var string
	 */
	protected $sLink = null;
	
	
	/**
	 * 
     * @param bool $pbSet
	 */
	public function setRestfulMod (bool $pbSet = true) : void
	{
		$this->bIsRestful = $pbSet;
	}
	
	
	/**
	 * 
     * @param bool $pbSet
	 */
	public function setConsoleMod (bool $pbSet = true) : void
	{
		$this->bIsConsole = $pbSet;
	}


	/**
	 * 
	 * @param string $psVar
	 * @param mixed $pmDefault
	 * @param bool $pbRequired
	 * @param string|null $psMsgHelp
	 * @return string|array|null
	 */
	public function getRequestArg (string $psVar, $pmDefault = null, bool $pbRequired = false, ?string $psMsgHelp = null)
	{
		return $this->oController->getRequestArg($psVar, $pmDefault, $pbRequired, $psMsgHelp);
	}


	/**
	 * 
	 * @param string $psMsg
	 */
	public function changedFileMsg (string $psMsg) : void
	{
	    echo ("\e[33m* File changed: " . $psMsg . "\e[0m\n");
	}
	
	
	/**
	 * 
	 * @param string $psFolder
	 * @return \OnionTool\Service\ToolServiceAbstract
	 */
	public function setProjectFolder (string $psFolder) : ToolServiceAbstract
	{
		$this->sProjectFolder = $psFolder;
		$this->sProjectPath = BASE_DIR . DS . 'client' . DS .strtolower($psFolder);
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psDomain
	 * @return \OnionTool\Service\ToolServiceAbstract
	 */
	public function setProjectDomain (string $psDomain) : ToolServiceAbstract
	{
		$this->sProjectDomain = $psDomain;
		
		if (empty($this->sProjectDomain))
		{
			$this->sProjectDomain = $this->sProjectFolder;
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psProject
	 * @return \OnionTool\Service\ToolServiceAbstract
	 */
	public function setProjectName (string $psProject) : ToolServiceAbstract
	{
		$this->sProjectName = $psProject;
		
		if (empty($this->sProjectName))
		{
			$laProjectFolder = explode(".", $this->sProjectFolder);
			$this->sProjectName = ucfirst($laProjectFolder[0]);
		}
		
		return $this;
	}
	
	
	/**
	 * 
	 * @param string $psModule
	 * @return \OnionTool\Service\ToolServiceAbstract
	 */
	public function setModuleName (string $psModule) : ToolServiceAbstract
	{
		if (!empty($psModule))
		{
			$this->sModuleName = ucfirst($psModule);
		}

		return $this;
	}
	
	
	/**
	 *
	 * @param string $psPath
	 * @param bool $pbHtaccess
	 * @param int|null $pnChmod
	 * @param string|null $psChown
	 * @param string|null $psChown
	 */
	public function createDir (string $psPath, bool $pbHtaccess = true, ?int $pnChmod = null, ?string $psChown = null, ?string $psChgrp = null) : void
	{
		try
		{
			System::createDir($psPath, $pnChmod, $psChown, $psChgrp);
		}
		catch (\Exception $e)
		{
			Prompt::echoError("Error creating directory: " . $psPath);
		}
		
		if ($pbHtaccess)
		{
			$lsFileContent = System::localRequest($this->sTemplatePath . DS . "deny.model");
			$lsFilePath = $psPath . DS . '.htaccess';

			try
			{
				System::saveFile($lsFilePath, $lsFileContent);
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsFilePath);
			}
		}
	}
	
	
	/**
	 *
	 * @param string $psPathConfig
	 */
	public function setModuleAutoload (string $psPathConfig) : void
	{
		$lsModulesPath = $psPathConfig . DS . "modules.php";
		
		if (file_exists($lsModulesPath))
		{
			$laModules = include ($lsModulesPath);
		
			$laModules['available'][$this->sModuleName] = true;
				
			$lsFileContent = ArrayObject::arrayToFile($laModules);
		
			try
			{
				if (System::saveFile($lsModulesPath, $lsFileContent))
				{
					$this->changedFileMsg($lsModulesPath);
				}
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsModulesPath);
			}
		}
	}
	
	
	/**
	 *
	 * @param string $psControllerName
	 */
	public function setMenu (string $psControllerName) : void
	{		
		$lsPathProject = $this->sProjectPath;
		$lsPathConfig = $lsPathProject . DS . 'config';
		$lsMenuPath = $lsPathConfig . DS . "menu.php";

		if (file_exists($lsMenuPath))
		{
			$laMenu = include ($lsMenuPath);
		
			$lsRoute = Str::slugfy($psControllerName);
			$lsModuleLabel = preg_replace("/_/", " ", ucfirst($lsRoute));
			
			$laMenu['admin'][$psControllerName] = [
				'resource' => $this->sModuleName . '\Controller\\' . $psControllerName . 'Controller',
				'action' => 'index',
				'accesskey' => '',
				'label' => $lsModuleLabel,
				'link' => "/{$lsRoute}",
				'description' => '',
				'icon' => 'material-icons rocket_launch',
				'submenu' => null,
			];
		
			$lsFileContent = ArrayObject::arrayToFile($laMenu);
		
			try
			{
				if (System::saveFile($lsMenuPath, $lsFileContent))
				{
					$this->changedFileMsg($lsMenuPath);
				}
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsMenuPath);
			}
		}
	}
	
	
	/**
	 * 
	 * @param string $psPathConfig
	 */
	public function setApiModuleAutoload (string $psPathConfig) : void
	{
		$lsApiModsAvailablePath = $psPathConfig . DS . "api-modsAvailable.php";
		$lsApiModulePath = $psPathConfig . DS . "api-module.php";
		
		if (file_exists($lsApiModsAvailablePath))
		{
			$laModulesAvailable = include ($lsApiModsAvailablePath);
			
			$laModulesAvailable[$this->sModuleName] = true;

			$lsFileContent = ArrayObject::arrayToFile($laModulesAvailable);
			
			try
			{
				if (System::saveFile($lsApiModsAvailablePath, $lsFileContent))
				{
					$this->changedFileMsg($lsApiModsAvailablePath);
				}
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsApiModsAvailablePath);
			}
		}
		elseif (file_exists($lsApiModulePath))
		{
			$laModuleLoader = include ($lsApiModulePath);
				
			if (is_array($laModuleLoader))
			{
				foreach ($laModuleLoader as $lsModuleAlias => $laValuePath)
				{
					$laModuleLoader[$lsModuleAlias . "\\"] = "//[CLIENT_DIR . DS . 'api' . DS . '{$lsModuleAlias}' . DS . 'src' . DS . {$lsModuleAlias}]";
				}
			}
			
			$laModuleLoader[$this->sModuleName . "\\"] = "//[CLIENT_DIR . DS . 'api' . DS . '{$this->sModuleName}' . DS . 'src' . DS . '{$this->sModuleName}']";
			$lsFileContent = ArrayObject::arrayToFile($laModuleLoader);

			try
			{
				if (System::saveFile($lsApiModulePath, $lsFileContent))
				{
					$this->changedFileMsg($lsApiModulePath);
				}
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsApiModulePath);
			}
		}
	}
	

	/**
	 *
	 * @param string $psModuleFolder
	 * @return string|null
	 */
	public function getModulePath (string $psModuleFolder = 'module') : ?string
	{
		$this->setProjectFolder($this->getRequestArg('project', "onionapp.com"));
		
		$lsPathProject = $this->sProjectPath;
		
		if (!file_exists($lsPathProject))
		{
			Prompt::exitError("Project folder do not exist! You need to create a new project first. Please, use --help for further information.");
			return null;
		}

		$this->setModuleName($this->getRequestArg('module', null, true));
		
		if ($this->sModuleName == null)
		{
			Prompt::echoError("The param module is required! Please, use --help for further information.");
			return null;
		}
		
		$lsPathModules = $lsPathProject . DS . $psModuleFolder;
		$lsPathModule = $lsPathModules . DS . $this->sModuleName;
		
		if (!file_exists($lsPathModule))
		{
			Prompt::exitError("Module folder do not exist! You need to create a new module first. Please, use --help for further information.");
			return null;
		}	

		return $lsPathModule;
	}


	/**
	 * 
	 * @param string $psPackage
	 * @return string
	 */
	public function getLicense (string $psPackage) : string
	{
		$laConfigTool = Config::loadConfigs();
		$this->sAuthor = $this->getRequestArg('author', $laConfigTool['defaults']['author']);
		$this->sEmail = $this->getRequestArg('email', $laConfigTool['defaults']['email']);
		$this->sLink = $this->getRequestArg('link', $laConfigTool['defaults']['link']);
		$lsCopyrightIni = $this->getRequestArg('cinit', $laConfigTool['defaults']['cinit']);
		
		$lsLicensePath = $this->sTemplatePath . DS . 'LICENSE.model';
		$lsFileLicense = System::localRequest($lsLicensePath);
		
		Util::parse($lsFileLicense, "#%PACKAGE%#", $psPackage);
		Util::parse($lsFileLicense, "#%AUTHOR%#", $this->sAuthor);
		Util::parse($lsFileLicense, "#%EMAIL%#", $this->sEmail);
		Util::parse($lsFileLicense, "#%LINK%#", $this->sLink);
		Util::parse($lsFileLicense, "#%COPYRIGHT%#", $lsCopyrightIni . "-" . date('Y'));
		
		return $lsFileLicense;
	}
	
	
	/**
	 * 
	 * @param string $psPath
	 * @param string $psFile
	 * @param string|null $psFileLicense
	 * @param string $psFileType
	 * @param string|null $psFilePartName
	 * @param bool $pbUseSufix
	 */
	public function saveFile (string $psPath, string $psFile, ?string $psFileLicense = "", string $psFileType = "php", ?string $psFilePartName = null, bool $pbUseSufix = false) : void
	{
		if (is_null($psFilePartName))
		{
			$psFilePartName = $this->sModuleName;
		}
		
		$lsSufix = "";

		if ($pbUseSufix && $this->bIsRestful)
		{
			$lsSufix = $this->sRestfulSufix;
		}
		elseif ($pbUseSufix && $this->bIsConsole)
		{
			$lsSufix = $this->sConsoleSufix;
		}

		switch ($psFile)
		{
			case 'htaccess':
				$lsFileName = ".{$psFile}";
				break;
			case 'Entity':
			case 'EntityEmpty':
				$lsFileName = "{$psFilePartName}.php";
				break;
			case 'actionIndex':
			case 'dashboard':
			case 'site':
				$lsFileName = "index.phtml";
				break;
			case '_Dashboard':
			case '_Site':
				$lsFileName = "{$psFilePartName}Controller.php";
			    break;
			case 'dashboard.config':
			case 'site.config':
			    $lsFileName = "module.config.php";
			    break;
			case 'action':
			    $lsFileName = "{$psFilePartName}.{$psFileType}";
				break;
			default:
				$lsFileName = "{$psFile}.{$psFileType}";
				$lsFileName = preg_replace("/_/", $psFilePartName.$lsSufix, $lsFileName);
				$lsFileName = preg_replace("/!|@|#|%/", "", $lsFileName);
				$lsFileName = preg_replace("/^console-/", "", $lsFileName);
				$lsFileName = preg_replace("/^restful-/", "", $lsFileName);
		}
		
		$lsFilePath = $psPath . DS . $lsFileName;

		if (!file_exists($lsFilePath))
		{
			$lsFileContent = System::localRequest($this->sTemplatePath . DS . "{$psFile}.model");
			
			Util::parse($lsFileContent, "#%LICENSE%#", $psFileLicense);
			Util::parse($lsFileContent, "#%MODULE%#", $this->sModuleName);
			Util::parse($lsFileContent, "#%CLASS%#", $psFilePartName);
			Util::parse($lsFileContent, "#%RESTFUL%#", ($this->bIsRestful ? $this->sRestfulSufix : ''));
			Util::parse($lsFileContent, "#%CONSOLE%#", ($this->bIsConsole ? $this->sConsoleSufix : ''));
			Util::parse($lsFileContent, "#%TABLE%#", $psFilePartName);
			Util::parse($lsFileContent, "#%ACTION%#", Str::lcfirst($psFilePartName));
			
			Util::parse($lsFileContent, "#%ROUTE%#", Str::slugfy($psFilePartName));
			
			Util::parse($lsFileContent, "#%PACKAGE%#", $this->sProjectFolder);
			Util::parse($lsFileContent, "#%CLIENT-NAME%#", $this->sProjectName);
			Util::parse($lsFileContent, "#%DATE%#", date('Y-m-d'));
			Util::parse($lsFileContent, "#%DOMAIN%#", $this->sProjectDomain);
			
			Util::parse($lsFileContent, "#%AUTHOR%#", $this->sAuthor);
			Util::parse($lsFileContent, "#%EMAIL%#", $this->sEmail);
			Util::parse($lsFileContent, "#%LINK%#", $this->sLink);

			$laFilePartName = explode('-', Str::slugfy($psFilePartName));
			$lsType = ucfirst(array_pop($laFilePartName));

			Util::parse($lsFileContent, "#%TYPE%#", $lsType);

			try
			{
				if (System::saveFile($lsFilePath, $lsFileContent))
				{
					Prompt::echoSuccess("File created: " . $lsFilePath);
				}
			}
			catch (\Exception $e)
			{
				Prompt::echoError("Error creating file: " . $lsFilePath);
			}
		}
	}
	
	
	/**
	 * 
	 * @return \OnionApi\Help
	 */
	public function showVHostConf () : Help
	{
		$lsDomain = "local.project-name";
		$lsDocumentRoot = "/var/www/path/to/the/client/public";
		$lsVirtualHost = $this->vHost($lsDocumentRoot, $lsDomain);
		$lsAppFolder = "/foo/bar/application/folder";
		
		$loHelp = new Help();
		$loHelp->clear();
		$loHelp->setAbout();
		$loHelp->setAppVersion("Onion Tool - Version: ", $this->oController->getVersion());

		$loHelp->set("    **** Apache 2.2 - Config development environment ****    \n", $loHelp::BROWN, "", $loHelp::B);
		
		$loHelp->setTopic("STEP 1");
		$loHelp->setLine("DocumentRoot", "Move or create a link of the application to the Apache document root (Linux default is /var/www)");
		$loHelp->setLine("Moving", "$ sudo mv {$lsAppFolder} /var/www/");
		$loHelp->setLine("Or");
		$loHelp->setLine("Simblink", "$ sudo ln -s {$lsAppFolder} /var/www/");
		
		$loHelp->setTopic("STEP 2");
		$loHelp->setLine("CHMOD project", "$ sudo chmod 755 {$lsAppFolder} -R");
		$loHelp->setLine("CHGPR project", "$ sudo chgrp www-data {$lsAppFolder} -R");
		
		$loHelp->setTopic("STEP 3");
		$loHelp->setLine("Set hosts file", "$ sudo echo 127.0.0.1	{$lsDomain} >> /etc/hosts");
		
		$loHelp->setTopic("STEP 4");
		$loHelp->setLine("Edit a new vhost", "$ sudo vi /etc/apache2/sites-available/{$lsDomain}.conf");
		$loHelp->setLine("Copy, paste & edit", $lsVirtualHost);
		
		$loHelp->setTopic("STEP 5");
		$loHelp->setLine("Active in sites-enable", '$ sudo a2ensite {$lsDomain}');
		
		$loHelp->setTopic("STEP 6");
		$loHelp->setLine("Reload Apache2", "$ sudo /etc/init.d/apache2 reload");

		return $loHelp;
	}
	

	/**
	 * 
	 * @param string $psController
	 */
	public function virtualHostDev (string $psController) : void
	{
		if ($_SERVER["USER"] == "root" || getEnv("USER") == "root" || Util::toBoolean($this->getRequestArg('force', "false")))
		{
			$laApacheConf = $this->getApacheConf();
			
			if (is_array($laApacheConf))
			{
				$lsApacheGrp = $laApacheConf['Group'];
				$lsDocRoot = $laApacheConf['DocumentRoot'] . DS;
			}
			else 
			{
				$lsApacheGrp = "www-data";
				$lsDocRoot = DS . "var" . DS . "www" . DS;
			}
			
			$lsApacheGroup = $this->getRequestArg('apachegrp', $lsApacheGrp);

			echo("CHGPR project folder\n");
			$laReturn = System::execute("chgrp {$lsApacheGroup} " . BASE_DIR . " -R");
			Debug::debug($laReturn);
				
			$lsDocRoot = $this->getRequestArg('docroot', $lsDocRoot, true);
			$laAppDirName = explode(DS, BASE_DIR);
			$lsAppDirName = array_pop($laAppDirName);
			
			if (!file_exists($lsDocRoot . DS . $lsAppDirName) || Prompt::confirm("The link {$lsDocRoot}{$lsAppDirName} already exists! Overwrite?"))
			{
				echo("Create project link to document root: " . BASE_DIR . "  to {$lsDocRoot}\n");
				System::simblink(BASE_DIR, $lsDocRoot);
			}
			
			if (Prompt::confirm("Create Apache virtual host?"))
			{
    			$lsProjectFolder = $this->getRequestArg('project', $this->sProjectFolder, true);

				if (empty($this->sProjectDomain))
			    {
			        $this->sProjectDomain = "local.{$lsProjectFolder}";
			    }
			    
				$lsDomain = $this->getRequestArg('domain', $this->sProjectDomain, true);
				$lsPort = $this->getRequestArg('httpport', '80', true);
				$lsHosts = $this->getRequestArg('hosts', DS . "etc" . DS . "hosts", true);
				$lsLocalhost = $this->getRequestArg('localhost', "127.0.0.1", true);
				$lsApacheConfDir = $this->getRequestArg('apachedir', DS . "etc" . DS . "apache2", true);
				
				if (file_exists($lsHosts))
				{
					echo("Backuping {$lsHosts} to {$lsHosts}.bkp\n");
					$laReturn = System::execute("cp {$lsHosts} {$lsHosts}.bkp");
					Debug::debug($laReturn);
					echo("Seting '{$lsLocalhost}\t{$lsDomain}' to {$lsHosts}\n");
					$laReturn = System::execute("echo {$lsLocalhost}\t{$lsDomain} >> {$lsHosts}");
					Debug::debug($laReturn);
				}
				else
				{
					Prompt::echoWarning("The {$lsHosts} does not exists!");
				}
                
				if ($psController == 'Zfs')
				{
				    $lsPublicFolder = 'public';
				}
				elseif ($psController == 'Api')
				{
				    $lsPublicFolder = 'public-api';
				}
				
				$lsDocumentRoot = $lsDocRoot . $lsAppDirName . DS . 'client' . DS . $lsProjectFolder . DS . $lsPublicFolder;
				$lsSitesAvailablePath = $lsApacheConfDir . DS . 'sites-available' . DS . "{$lsDomain}.conf";
				$lsSitesEnabledPath = $lsApacheConfDir . DS . 'sites-enabled' . DS . "{$lsDomain}.conf";

				if (!file_exists($lsSitesAvailablePath) || Prompt::confirm("The vhost {$lsDomain}.conf already exists! Overwrite?"))
				{
					echo("Creating vhost: {$lsSitesAvailablePath}\n");
					$lsVirtualHost = $this->vHost($lsDocumentRoot, $lsDomain, $lsPort);
					Debug::debug($lsVirtualHost);
					$laReturn = System::saveFile($lsSitesAvailablePath, trim($lsVirtualHost));
					Debug::debug($laReturn);
				}
				
				echo("Enable vhost {$lsDomain}\n");
				$laReturn = System::execute("ln -s {$lsSitesAvailablePath} {$lsSitesEnabledPath}");
				Debug::debug($laReturn);
			
				if (Util::toBoolean($this->getRequestArg('apacherestart', 'true')))
				{
					echo("Apache restart\n");

					try {
						if ($this->checkCommandLine('apachectl'))
						{
							$laReturn = System::execute("apachectl -k restart");
						}
						elseif ($this->checkCommandLine('httpd'))
						{
							$laReturn = System::execute("httpd -k restart");
						}
					}
					catch (\Exception $e) {
						Prompt::echoWarning("APACHE not instaled");
					}

					Debug::debug($laReturn);
				}
			}
		}
		else
		{
			Prompt::echoError("You need root access to run this action! Please try run this action using sudo.");
		}
	}
	
	
	/**
	 * 
	 * @param string $psController
	 */
	public function uninstallProject (string $psController) : void
	{
		if ($_SERVER["USER"] == "root" || getEnv("USER") == "root" || Util::toBoolean($this->getRequestArg('force', "false")))
		{
			$this->setProjectFolder($this->getRequestArg('project', null, true));
		
			if ($psController == 'Zfs')
			{
		        $lsDbPath = $this->sProjectPath . DS . 'config' . DS . 'db.php';
		        $laDbProjectConf = require($lsDbPath);
		        
			    $laDbConf = [
		            'driver' => $laDbProjectConf['production']['driver'],
		            'charset' => $laDbProjectConf['production']['charset'],				            
        			'hostname' => $laDbProjectConf['production']['hostname'],
        			'port' => $laDbProjectConf['production']['port'],
        			'username' => $laDbProjectConf['production']['username'],
        			'password' => $laDbProjectConf['production']['password'],
			        'database' => $laDbProjectConf['production']['database'],
        		];		        
			}
		    
		    if (Prompt::confirm("Remove project folder {$this->sProjectFolder}?"))
		    {
		        System::removeDir($this->sProjectPath);
		    }
		    
			$laApacheConf = $this->getApacheConf();
			
			if (is_array($laApacheConf))
			{
				$lsDocRoot = $laApacheConf['DocumentRoot'] . DS;
			}
			else 
			{
				$lsDocRoot = DS . "var" . DS . "www" . DS;
			}
			
			if (Prompt::confirm("Remove link from document root?"))
			{
			    $lsDocRoot = $this->getRequestArg('docroot', $lsDocRoot, true);
			    $laAppDirName = explode(DS, BASE_DIR);
			    $lsAppDirName = array_pop($laAppDirName);
			    
				System::removeSimblink($lsDocRoot . DS . $lsAppDirName);
			}
			
			if (Prompt::confirm("Remove Apache virtual host?"))
			{
			    $lsDomain = $this->getRequestArg('domain', "local.{$this->sProjectFolder}", true);
				$lsApacheConfDir = $this->getRequestArg('apachedir', DS . "etc" . DS . "apache2" . DS, true);
				
				$lsSitesAvailablePath = $lsApacheConfDir . 'sites-available' . DS . "{$lsDomain}.conf";
				$lsSitesEnablePath = $lsApacheConfDir . 'sites-enabled' . DS . "{$lsDomain}.conf";
				
			    System::removeFile($lsSitesAvailablePath);
			    System::removeSimblink($lsSitesEnablePath);
				
				if (Util::toBoolean($this->getRequestArg('apacherestart', 'true')))
				{
					echo("Apache restart\n");

					try {
						if ($this->checkCommandLine('apachectl'))
						{
							$laReturn = System::execute("apachectl -k restart");
						}
						elseif ($this->checkCommandLine('httpd'))
						{
							$laReturn = System::execute("httpd -k restart");
						}
					}
					catch (\Exception $e) {
						Prompt::echoWarning("APACHE not instaled");
					}

					Debug::debug($laReturn);
				}
			}
			
			if ($psController == 'Zfs' && Prompt::confirm("Drop database?", "n"))
			{
        		$this->aRepository['db'] = new InstallRepository($laDbConf);
        		
        		if ($this->aRepository['db']->connect())
        		{
        		    if (Prompt::confirm("Confirm drop database {$laDbConf['hostname']}:{$laDbConf['database']}?"))
			        {
            			if (!$this->aRepository['db']->dropDb($laDbConf['database']))
            			{
    				        Prompt::echoWarning("There is something wrong!");
    				        return;        			    
            			}
			        }
        		}
			}
		}
		else
		{
			Prompt::echoError("You need root access to run this action! Please try run this action using sudo.");
		}	    
	}
	
	
	/**
	 * 
	 * @param string $psDocRoot
	 * @param string $psDomain
	 * @param string $psPort
	 * @return string
	 */
	public function vHost (string $psDocRoot, string $psDomain, string $psPort = "80") : string
	{
		$lsVirtualHost = '
		<VirtualHost *:' . $psPort . '>
			ServerName ' . $psDomain . '
			ServerAdmin webmaster@localhost
					
			DocumentRoot ' . $psDocRoot . '
					
			<Directory />
				Options FollowSymLinks
				AllowOverride None
			</Directory>
			<Directory ' . $psDocRoot . '/>
				Options Indexes FollowSymLinks MultiViews
				AllowOverride All
				Order allow,deny
				allow from all
			</Directory>
			
			#ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
			#<Directory "/usr/lib/cgi-bin">
			#	AllowOverride None
			#	Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
			#	Order allow,deny
			#	Allow from all
			#</Directory>
			
			ErrorLog ${APACHE_LOG_DIR}/error.log
			
			# Possible values include: debug, info, notice, warn, error, crit,
			# alert, emerg.
			LogLevel warn
			
			CustomLog ${APACHE_LOG_DIR}/access.log combined
		</VirtualHost>
		';
        
        return $lsVirtualHost;
    }

    
    /**
	 * 
	 * @return bool
     */
    public function checkEnv () : bool
    {
        $lbCheck = true;
        
        $laPHPExtensions = [
			"bcmath",
            "bz2",
            "calendar",
			"Core",
			"curl",
			"date",
			"dom",
			"fileinfo",
			"gd",
            "hash",
			"intl",
			"json",
			"libxml",
			"mbstring",
			"memcached",
			"OAuth",
			"PDO",
			"pdo_mysql",
			"pdo_pgsql",
			"pdo_sqlite",
			"random",
			"Reflection",
			"session",
        	"SimpleXML",
			"sockets",
			"tokenizer",
			"xdebug",
			"xml",
			"xmlreader",
			"xmlwriter",
			"Zend OPcache",
			"zip",
			"zlib",
		];
		
        Debug::debug(get_loaded_extensions());
        
		if (version_compare(phpversion(), '5.4', '>='))
		{
			Prompt::echoSuccess("PHP " . phpversion() . " - OK");
			
			foreach ($laPHPExtensions as $lsExtension)
			{
				if (in_array($lsExtension, get_loaded_extensions()))
				{
					Prompt::echoSuccess("PHP extension {$lsExtension} - ENABLE");
				}
				else 
				{
					Prompt::echoWarning("PHP extension {$lsExtension} - DISABLE");
					$lbCheck = false;
				}
			}
			
			if (ini_get('allow_url_fopen'))
			{
				Prompt::echoSuccess("PHP fopen - ENABLE");
			}
			else 
			{
				Prompt::echoWarning("PHP - DISABLED");
				$lbCheck = false;
			}
			
			$laApacheReturn = null;

			try {
				if ($this->checkCommandLine('apachectl'))
				{
					$laApacheReturn = System::execute("apachectl -M");
				}
				elseif ($this->checkCommandLine('httpd'))
				{
					$laApacheReturn = System::execute("httpd -M");
				}
			}
			catch (\Exception $e) {
				Prompt::echoWarning("APACHE not instaled");
			}

			Debug::debug($laApacheReturn);
			
			$laModules = [
				'mpm_prefork' => false,
				'mpm_worker' => false,
				'mpm_event' => false,
			    'php' => false,
				'php-fpm' => false,
				'proxy_fcgi' => false,
		        'rewrite' => false,
		        'deflate' => false,
		        'expires' => false,
		        'headers' => false,
			];
			
			if (is_array($laApacheReturn))
			{
				foreach ($laApacheReturn as $lsLine)
				{
					if (strpos($lsLine, 'mpm_prefork'))
					{
						Prompt::echoSuccess("Apache2 mpm_prefork - ENABLE");
						$laModules['mpm_prefork'] = true; 
					}
					elseif (strpos($lsLine, 'mpm_worker'))
					{
						Prompt::echoSuccess("Apache2 mpm_worker - ENABLE");
						$laModules['mpm_worker'] = true; 
					}
					elseif (strpos($lsLine, 'mpm_event'))
					{
						Prompt::echoSuccess("Apache2 mpm_event - ENABLE");
						$laModules['mpm_event'] = true; 
					}
					elseif (strpos($lsLine, 'php'))
					{
					    Prompt::echoSuccess("Apache2 mod_php - ENABLE");
						$laModules['php'] = true; 
					}
					elseif (strpos($lsLine, 'fpm'))
					{
					    Prompt::echoSuccess("Apache2 mod_php-fpm - ENABLE");
						$laModules['php-fpm'] = true; 
					}
					elseif (strpos($lsLine, 'fcgi'))
					{
					    Prompt::echoSuccess("Apache2 mod_proxy-fcgi - ENABLE");
						$laModules['proxy-fcgi'] = true; 
					}
					elseif (strpos($lsLine, 'deflate'))
					{
						Prompt::echoSuccess("Apache2 mod_deflate - ENABLE");
						$laModules['deflate'] = true; 
					}
					elseif (strpos($lsLine, 'expires'))
					{
					    Prompt::echoSuccess("Apache2 mod_expires - ENABLE");
						$laModules['expires'] = true; 
					}
					elseif (strpos($lsLine, 'headers'))
					{
					    Prompt::echoSuccess("Apache2 mod_headers - ENABLE");
						$laModules['headers'] = true; 
					}
					elseif (strpos($lsLine, 'rewrite'))
					{
					    Prompt::echoSuccess("Apache2 mod_rewrite - ENABLE");
						$laModules['rewrite'] = true; 
					}
				}
			}
			
			foreach ($laModules as $lsModule => $lbOk)
			{
			    if (!$lbOk)
			    {
				    Prompt::echoWarning("Apache2 mod_{$lsModule} - DISABLED");
				    $lbCheck = false;
			    }
			}
			
			Prompt::echoCommand("Enable PHP mods:", "$ sudo phpenmod module-name");
			Prompt::echoCommand("Disable PHP mods:", "$ sudo phpdismod module-name");
			Prompt::echoCommand("Enable Apache mods:", "$ sudo a2enmod module-name");
			Prompt::echoCommand("Disable Apache mods:", "$ sudo a2dismod module-name");
		}
		else
		{
			Prompt::echoWarning('You need PHP version 5.4 or latest!');
			$lbCheck = false;
		}
		
		return $lbCheck;
	}
	
	
	/**
	 * 
	 * @return array
	 */
	public function getApacheConf () : array 
	{
		$laApache = [];

	    if ($this->checkCommandLine('apachectl'))
	    {
		    $laApacheReturn = System::execute("apachectl -t -D DUMP_RUN_CFG");
	    }
	    elseif ($this->checkCommandLine('httpd'))
	    {
		    $laApacheReturn = System::execute("httpd -t -D DUMP_RUN_CFG");
	    }	    
	    
		Debug::debug($laApacheReturn);
		
		if (is_array($laApacheReturn))
		{
			foreach ($laApacheReturn as $lsLine)
			{
				$laLine = explode(":", $lsLine);
				
				switch ($laLine[0])
				{
					case "ServerRoot":
						preg_match("/\"(.*?)\"/", $laLine[1], $laMatch);
						$laApache["ServerRoot"] = $laMatch[1];
						break;
					case "Main DocumentRoot":
						preg_match("/\"(.*?)\"/", $laLine[1], $laMatch);
						$laApache["DocumentRoot"] = $laMatch[1];
						break;
					case "Group":
						preg_match("/name=\"(.*?)\" id=/", $laLine[1], $laMatch);
						$laApache["Group"] = $laMatch[1];
						break;
				}
			}
		}
		
		Debug::debug($laApache);
		
		return $laApache;
	}
	
	
	/**
	 * 
	 * @param string $psCommand
	 * @return bool
	 */
	public function checkCommandLine (string $psCommand) : bool
	{
	    $laCommandReturn = System::execute("command -v {$psCommand}");
		Debug::debug($laCommandReturn);
	    
		if (is_array($laCommandReturn) && count($laCommandReturn) > 0)
		{
		    return true;
		}
		
		return false;
	}


	/**
	 * 
	 * @param string $psConfig
	 * @return array|null
	 */
	public function getProjectConf (string $psConfig = 'project') : ?array
	{
		$laConfig = null;
		$lsConfLocalPath = $this->sProjectPath . DS . 'config' . DS . "{$psConfig}.local.php";
			
		if (file_exists($lsConfLocalPath))
		{
			$laConfigLocal = include $lsConfLocalPath;
		}

		$lsConfigPath = $this->sProjectPath . DS . 'config' . DS . "{$psConfig}.php";

		if (file_exists($lsConfigPath))
		{
			$laConfig = include $lsConfigPath;
		}
			
		if (isset($laConfigLocal) && is_array($laConfigLocal))
		{
			$laConfig = array_merge($laConfig, $laConfigLocal); 
		}

		return $laConfig;
	}
}