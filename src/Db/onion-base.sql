/*M!999999 - enable the sandbox mode */ 

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*M!100616 SET @OLD_NOTE_VERBOSITY=@@NOTE_VERBOSITY, NOTE_VERBOSITY=0 */;
CREATE DATABASE IF NOT EXISTS `onionapp` 
/*!40100 DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci */ 
/*!80016 DEFAULT ENCRYPTION='N' */;

DROP TABLE IF EXISTS `LogEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LogEvent` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stPriority` varchar(1) DEFAULT NULL,
  `stIP` varchar(20) DEFAULT NULL,
  `stResource` varchar(255) DEFAULT NULL,
  `stAction` varchar(100) DEFAULT NULL,
  `Resource_id` int unsigned DEFAULT NULL,
  `Resource_hash` varchar(100) DEFAULT NULL,
  `stMessage` longtext,
  `txtServer` text NOT NULL,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`),
  CONSTRAINT `LogEvent_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysAccessToken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysAccessToken` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stToken` varchar(255) NOT NULL,
  `stLabel` varchar(255) DEFAULT NULL,
  `stResource` varchar(100) DEFAULT NULL,
  `stAction` varchar(100) NOT NULL,
  `Resource_id` int unsigned NOT NULL,
  `Resource_hash` varchar(256) NOT NULL,
  `stEmail` varchar(255) NOT NULL,
  `stName` varchar(150) NOT NULL,
  `txtMessage` text NOT NULL,
  `dtLastAccess` datetime DEFAULT NULL,
  `dtValidThru` datetime DEFAULT NULL,
  `txtData` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `stToken` (`stToken`),
  KEY `stEmail` (`stEmail`),
  KEY `stLabel` (`stLabel`),
  KEY `SysAccount_id` (`SysAccount_id`),  
  CONSTRAINT `SysAccessToken_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysAccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysAccount` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysPlan_id` int unsigned DEFAULT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stAccountName` varchar(100) DEFAULT NULL,
  `enumType` enum('company','user') DEFAULT NULL,
  `SysUser_id` int unsigned DEFAULT NULL,
  `SysCompany_id` int unsigned DEFAULT NULL,
  `SysPerson_id` int unsigned DEFAULT NULL,
  `txtAgreement` text,
  `hasAgreement` enum('0','1') DEFAULT NULL,
  `txtResources` text,
  `txtFinancial` text,
  `txtConfig` text,
  `txtConfigRoot` text,
  `txtLimit` text,
  `txtResource` text,
  `txtData` text,
  `txtObservation` text,
  `numCreditBalance` decimal(12,3) NOT NULL DEFAULT '0.000',
  `numPromotionalBalance` decimal(12,3) NOT NULL DEFAULT '0.000',
  `isWhiteLabel` enum('0','1') NOT NULL DEFAULT '0',
  `stDomain` varchar(256) DEFAULT NULL,
  `SysAccountWithAccess_id` int unsigned DEFAULT NULL,
  `isSystem` enum('0','1') NOT NULL DEFAULT '0',
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysPlan_id` (`SysPlan_id`),
  KEY `enumType` (`enumType`),
  KEY `SysUser_id` (`SysUser_id`),
  KEY `SysCompany_id` (`SysCompany_id`),
  KEY `SysPerson_id` (`SysPerson_id`),
  KEY `SysAccount_id` (`SysAccount_id`),  
  KEY `SysAccountWithAccess_id` (`SysAccountWithAccess_id`),
  CONSTRAINT `SysAccount_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SysAccount_ibfk_2` FOREIGN KEY (`SysPlan_id`) REFERENCES `SysPlan` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `SysAccount_ibfk_4` FOREIGN KEY (`SysUser_id`) REFERENCES `SysUser` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `SysAccount_ibfk_5` FOREIGN KEY (`SysCompany_id`) REFERENCES `SysCompany` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `SysAccount_ibfk_6` FOREIGN KEY (`SysPerson_id`) REFERENCES `SysPerson` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysAddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysAddress` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `Resource_id` int unsigned NOT NULL,
  `Resource_hash` varchar(255) DEFAULT NULL,
  `stPlace` varchar(250) DEFAULT NULL,
  `isMain` enum('0','1') NOT NULL DEFAULT '0',
  `stCountry` varchar(100) DEFAULT NULL,
  `stEstate` varchar(100) DEFAULT NULL,
  `stCity` varchar(255) DEFAULT NULL,
  `stNeigborhood` varchar(255) DEFAULT NULL,
  `stStreet` varchar(255) DEFAULT NULL,
  `stZipCode` varchar(10) DEFAULT NULL,
  `stExtraCode` varchar(20) DEFAULT NULL,
  `stNumber` varchar(10) DEFAULT NULL,
  `stComplement` varchar(255) DEFAULT NULL,
  `stGeoLocalization` varchar(100) DEFAULT NULL,
  `stZoomMap` varchar(3) DEFAULT NULL,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`),
  KEY `Resource_hash` (`Resource_hash`),  
  CONSTRAINT `SysAddress_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysAuthLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysAuthLog` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUser_id` int unsigned DEFAULT NULL,
  `stUserName` varchar(255) DEFAULT NULL,
  `SysUserGroup_id` int unsigned DEFAULT NULL,
  `stPriority` varchar(1) DEFAULT NULL,
  `stSession` varchar(255) DEFAULT NULL,
  `stIP` varchar(20) DEFAULT NULL,
  `stMessage` varchar(255) DEFAULT NULL,
  `stUserAgent` varchar(255) DEFAULT NULL,
  `txtServer` text NOT NULL,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`),
  CONSTRAINT `SysAuthLog_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysCompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysCompany` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `SysCompany_id` int unsigned DEFAULT NULL,
  `stProfile` varchar(50) DEFAULT NULL,
  `isMain` enum('0','1') NOT NULL DEFAULT '1',
  `stName` varchar(250) NOT NULL,
  `stAliasName` varchar(250) DEFAULT NULL,
  `stRegistry` varchar(25) DEFAULT NULL,
  `stRegistry2` varchar(25) DEFAULT NULL,
  `stArea` varchar(255) DEFAULT NULL,
  `stLogo` varchar(250) DEFAULT NULL,
  `SysContactEmail_id` int unsigned DEFAULT NULL,
  `SysContactPhone_id` int unsigned DEFAULT NULL,
  `SysAddress_id` int unsigned DEFAULT NULL,
  `txtData` text,
  `txtObservation` text,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `SysAccount_registry` (`SysAccount_id`,`stRegistry`) USING BTREE,
  KEY `stProfile` (`stProfile`),
  KEY `stName` (`stName`),  
  KEY `SysCompany_id` (`SysCompany_id`),
  KEY `SysContactEmail_id` (`SysContactEmail_id`),
  KEY `SysContactPhone_id` (`SysContactPhone_id`),
  KEY `SysAddress_id` (`SysAddress_id`),
  CONSTRAINT `SysCompany_ibfk_2` FOREIGN KEY (`SysCompany_id`) REFERENCES `SysCompany` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysCompany_ibfk_3` FOREIGN KEY (`SysContactEmail_id`) REFERENCES `SysContact` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysCompany_ibfk_4` FOREIGN KEY (`SysContactPhone_id`) REFERENCES `SysContact` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysCompany_ibfk_5` FOREIGN KEY (`SysAddress_id`) REFERENCES `SysAddress` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysCompany_ibfk_6` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysContact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysContact` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `Resource_id` int unsigned NOT NULL,
  `Resource_hash` varchar(255) DEFAULT NULL,
  `stType` varchar(20) DEFAULT NULL,
  `stLabel` varchar(100) DEFAULT NULL,
  `stValue` varchar(250) DEFAULT NULL,
  `stPerson` varchar(250) DEFAULT NULL,
  `isMain` enum('0','1') DEFAULT '0',
  `isConfirmed` enum('0','1','2') NOT NULL DEFAULT '0',
  `dtConfirmed` datetime DEFAULT NULL,
  `stToken` varchar(100) DEFAULT NULL,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `Resource_hash` (`Resource_hash`),
  KEY `SysAccount_id` (`SysAccount_id`),
  KEY `stType` (`stType`),
  KEY `stValue` (`stValue`),  
  CONSTRAINT `SysContact_ibfk_2` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysNotification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysNotification` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) DEFAULT NULL,
  `stAction` varchar(100) DEFAULT NULL,
  `stChannel` varchar(30) NOT NULL,
  `stApiAdapter` varchar(100) DEFAULT NULL,
  `stApiName` varchar(100) NOT NULL,
  `stApiVersion` varchar(20) DEFAULT NULL,
  `stSenderName` varchar(256) DEFAULT NULL,
  `stSenderId` varchar(256) DEFAULT NULL,
  `stDescription` varchar(256) DEFAULT NULL,
  `stServer` varchar(256) DEFAULT NULL,
  `numPort` mediumint unsigned DEFAULT NULL,
  `enumProtocol` enum('HTTP','HTTPS','SMTP','XMPP','ICMP') NOT NULL DEFAULT 'HTTPS',
  `stLogin` varchar(256) DEFAULT NULL,
  `stPassword` varchar(256) DEFAULT NULL,
  `stToken` text,
  `txtConfig` text,
  `txtAdmConfig` text,
  `isSystem` enum('0','1') DEFAULT '0',
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`),
  KEY `stMedia` (`stChannel`),  
  KEY `stResource` (`stResource`),
  KEY `stAction` (`stAction`),
  KEY `stSenderId` (`stSenderId`),
  KEY `stApiAdapter` (`stApiAdapter`),
  CONSTRAINT `SysNotification_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysNotificationCampaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysNotificationCampaign` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stChannel` varchar(30) NOT NULL,
  `stSubject` varchar(256) NOT NULL,
  `txtMsg` text NOT NULL,
  `enumFileMediaType` enum('image','video','audio','document','link') DEFAULT NULL,
  `stFileMedia` varchar(256) DEFAULT NULL,
  `stLink` varchar(256) DEFAULT NULL,
  `dtStart` datetime DEFAULT NULL,
  `dtEnd` datetime DEFAULT NULL,
  `txtFilter` text,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`),
  CONSTRAINT `SysNotificationCampaign_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysNotificationRecipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysNotificationRecipient` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `SysNotificationCampaign_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `stAction` varchar(100) NOT NULL,
  `stItemHash` varchar(255) DEFAULT NULL,
  `enumPriority` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
  `stChannel` varchar(30) NOT NULL,
  `stRecipientType` varchar(20) DEFAULT NULL,
  `SysPerson_id` int unsigned DEFAULT NULL,
  `SysContact_id` int unsigned DEFAULT NULL,
  `stRecipient` varchar(255) DEFAULT NULL,
  `stContact` varchar(256) DEFAULT NULL,
  `stSubject` varchar(255) DEFAULT NULL,
  `txtMsg` text NOT NULL,
  `txtData` text,
  `stLink` varchar(256) DEFAULT NULL,
  `stFileMedia` varchar(256) DEFAULT NULL,
  `enumFileMediaType` enum('image','video','audio','document','link') DEFAULT NULL,
  `isDelivered` enum('0','1') NOT NULL DEFAULT '0',
  `SysNotificationAPI_id` int DEFAULT NULL,
  `txtResponse` text,
  `stResponseCode` varchar(100) DEFAULT NULL,
  `stConversationId` varchar(255) DEFAULT NULL,
  `txtView` text,
  `txtClick` text,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,
  KEY `stMedia` (`stChannel`),
  KEY `numStatus` (`numStatus`),
  KEY `stItemHash` (`stItemHash`),  
  KEY `SysPerson_id` (`SysPerson_id`) USING HASH,
  KEY `dtInsert` (`dtInsert`),
  KEY `SysNotificationCampaign_id` (`SysNotificationCampaign_id`) USING HASH,
  KEY `SysNotificationAPI_id` (`SysNotificationAPI_id`) USING HASH,
  KEY `SysNotificationAPI_id_2` (`SysNotificationAPI_id`) USING HASH,
  KEY `SysContact_id` (`SysContact_id`) USING HASH,
  CONSTRAINT `SysNotificationRecipient_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SysNotificationRecipient_ibfk_3` FOREIGN KEY (`SysPerson_id`) REFERENCES `SysPerson` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `SysNotificationRecipient_ibfk_4` FOREIGN KEY (`SysNotificationCampaign_id`) REFERENCES `SysNotificationCampaign` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `SysNotificationRecipient_ibfk_5` FOREIGN KEY (`SysContact_id`) REFERENCES `SysContact` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysNotificationTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysNotificationTemplate` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `stAction` varchar(100) NOT NULL,
  `stChannel` varchar(30) NOT NULL,
  `enumPriority` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
  `stSubject` varchar(256) DEFAULT NULL,
  `txtMsg` text NOT NULL,
  `stLink` varchar(256) DEFAULT NULL,
  `stFileMedia` varchar(256) DEFAULT NULL,
  `enumFileMediaType` enum('image','video','audio','document','link') DEFAULT NULL,
  `isSystem` enum('0','1') NOT NULL DEFAULT '0',
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,
  KEY `stResource` (`stResource`) USING HASH,
  KEY `stAction` (`stAction`),
  KEY `stMedia` (`stChannel`),  
  CONSTRAINT `SysNotificationTemplate_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysOptions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) DEFAULT NULL,
  `stGroup` varchar(100) DEFAULT NULL,
  `txtValues` text NOT NULL,
  `stLocale` varchar(8) DEFAULT 'pt-br',
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `stResource` (`stResource`) USING HASH,
  KEY `stGroup` (`stGroup`),
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,  
  CONSTRAINT `SysOptions_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysPerson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysPerson` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stProfile` varchar(50) DEFAULT NULL,
  `stName` varchar(255) NOT NULL,
  `stSocialName` varchar(255) DEFAULT NULL,
  `stCitizenId` varchar(30) NOT NULL,
  `stCitizenId2` varchar(30) DEFAULT NULL,
  `stDriverLicence` varchar(20) DEFAULT NULL,
  `dtDriverLicenceValidThru` date DEFAULT NULL,
  `stDriverLicenceCategory` varchar(20) DEFAULT NULL,
  `isForeigner` enum('0','1') NOT NULL DEFAULT '0',
  `stNationality` varchar(50) DEFAULT NULL,
  `dtBirthdate` date DEFAULT NULL,
  `isChild` enum('0','1') NOT NULL DEFAULT '0',
  `enumGender` enum('M','F') DEFAULT NULL,
  `stOrientation` varchar(40) DEFAULT NULL,
  `stPicture` varchar(255) DEFAULT NULL,
  `SysContactEmail_id` int unsigned DEFAULT NULL,
  `SysContactPhone_id` int unsigned DEFAULT NULL,
  `SysAddress_id` int unsigned DEFAULT NULL,
  `txtData` text,
  `txtPref` text,
  `enumAcceptLgpd` enum('0','1') NOT NULL DEFAULT '0',
  `txtObservation` text,
  `txtNote` text,
  `txtAttachedFiles` text,
  `dtBlocked` datetime DEFAULT NULL,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `SysAccount_CitizenId` (`SysAccount_id`,`stCitizenId`),
  KEY `stCitizenId` (`stCitizenId`),
  KEY `stName` (`stName`),
  KEY `stSocialName` (`stSocialName`),
  KEY `SysContactEmail_id` (`SysContactEmail_id`) USING HASH,
  KEY `SysContactPhone_id` (`SysContactPhone_id`) USING HASH,  
  KEY `SysAddress_id` (`SysAddress_id`),
  KEY `dtBlocked` (`dtBlocked`),
  CONSTRAINT `SysPerson_ibfk_2` FOREIGN KEY (`SysContactEmail_id`) REFERENCES `SysContact` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysPerson_ibfk_3` FOREIGN KEY (`SysContactPhone_id`) REFERENCES `SysContact` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysPerson_ibfk_4` FOREIGN KEY (`SysAddress_id`) REFERENCES `SysAddress` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `SysPerson_ibfk_5` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysPlan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysPlan` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stPlan` varchar(255) NOT NULL,
  `stTitle` varchar(255) DEFAULT NULL,
  `stProfile` varchar(50) NOT NULL,
  `SysUserGroup_id` smallint unsigned NOT NULL,
  `txtDescription` text,
  `stIcon` varchar(255) DEFAULT NULL,
  `stColor` varchar(255) DEFAULT NULL,
  `numValue` decimal(10,2) unsigned DEFAULT NULL,
  `numOldValue` decimal(10,2) unsigned DEFAULT NULL,
  `numDiscount` decimal(10,2) unsigned DEFAULT NULL,
  `isPromotion` enum('0','1') NOT NULL DEFAULT '0',
  `isFeatured` enum('0','1') NOT NULL DEFAULT '0',
  `isSystem` enum('0','1') NOT NULL DEFAULT '0',
  `stIntegrationKey` varchar(255) DEFAULT NULL,
  `txtDetails` text,
  `txtAttributes` text,
  `txtParams` text,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,  
  KEY `SysUserGroup_id` (`SysUserGroup_id`) USING HASH,
  CONSTRAINT `SysPlan_ibfk_2` FOREIGN KEY (`SysUserGroup_id`) REFERENCES `SysUserGroup` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysTabTemp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysTabTemp` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `stAction` varchar(255) NOT NULL,
  `stToken` varchar(255) NOT NULL,
  `txtData` longtext,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,  
  CONSTRAINT `SysTabTemp_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysTags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `stResource` varchar(100) NOT NULL,
  `stType` varchar(50) NOT NULL,
  `stTag` varchar(100) NOT NULL,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `Tag` (`SysAccount_id`,`stResource`,`stType`,`stTag`),
  KEY `stResource` (`stResource`),
  KEY `stTag` (`stTag`),
  KEY `stType` (`stType`),
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,  
  CONSTRAINT `SysTags_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysUser` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `SysUserGroup_id` smallint unsigned DEFAULT NULL,
  `SysPerson_id` int unsigned DEFAULT NULL,
  `stProfile` varchar(50) DEFAULT NULL,
  `stUsername` varchar(250) NOT NULL,
  `stPassword` varchar(250) DEFAULT NULL,
  `stPasswordSalt` varchar(255) DEFAULT NULL,
  `stRegistrationToken` varchar(255) DEFAULT NULL,
  `txtAccessContext` text,
  `txtUserPermission` text,
  `txtAssetsPermission` text,
  `txtUserPref` text,
  `txtData` text,
  `stBranchLine` varchar(20) DEFAULT NULL,
  `isSystem` enum('0','1') DEFAULT '0',
  `isContactConfirmed` enum('0','1') NOT NULL DEFAULT '0',
  `stExternalCode` varchar(255) DEFAULT NULL,
  `stOrigin` varchar(20) DEFAULT NULL,
  `txtTag` text,
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `stUsername` (`stUsername`),
  KEY `stPassword` (`stPassword`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,
  KEY `SysPerson_id` (`SysPerson_id`) USING HASH,
  KEY `SysUserGroup_id` (`SysUserGroup_id`) USING HASH,  
  KEY `stRegistrationToken` (`stRegistrationToken`) USING HASH,
  CONSTRAINT `SysUser_ibfk_2` FOREIGN KEY (`SysUserGroup_id`) REFERENCES `SysUserGroup` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `SysUser_ibfk_3` FOREIGN KEY (`SysPerson_id`) REFERENCES `SysPerson` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SysUser_ibfk_4` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `SysUserGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SysUserGroup` (
  `id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `stHash` varchar(100) NOT NULL,
  `SysAccount_id` int unsigned NOT NULL,
  `SysUserOwner_id` int unsigned DEFAULT NULL,
  `SysUserGroup_id` smallint unsigned DEFAULT NULL,
  `stAccountProfile` varchar(30) DEFAULT NULL,
  `stName` varchar(45) NOT NULL,
  `stLabel` varchar(50) NOT NULL,
  `txtAccessContext` text,
  `txtGroupPermission` text,
  `txtAssetsPermission` text,
  `txtGroupPref` text,
  `numHierarchy` tinyint unsigned NOT NULL DEFAULT '0',
  `stDashboard` varchar(256) DEFAULT NULL,
  `stMenu` varchar(256) DEFAULT NULL,
  `isSystem` enum('0','1') DEFAULT '0',
  `dtInsert` timestamp NULL DEFAULT NULL,
  `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
  `isActive` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stHash` (`stHash`) USING HASH,
  UNIQUE KEY `stName` (`stName`),
  KEY `SysUserGroup_id` (`SysUserGroup_id`) USING HASH,
  KEY `SysAccount_id` (`SysAccount_id`) USING HASH,  
  CONSTRAINT `SysUserGroup_ibfk_1` FOREIGN KEY (`SysAccount_id`) REFERENCES `SysAccount` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SysUserGroup_ibfk_3` FOREIGN KEY (`SysUserGroup_id`) REFERENCES `SysUserGroup` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3  COLLATE utf8mb3_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


START TRANSACTION;

LOCK TABLES `SysPlan` WRITE;
/*!40000 ALTER TABLE `SysPlan` DISABLE KEYS */;
INSERT INTO `SysPlan` (`stHash`, `SysUserOwner_id`, `stPlan`, `stTitle`, `stProfile`, `SysUserGroup_id`, `isSystem`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_plan_a', 1, 'Onion', 'Onion', 'Personal', 2, '1', 0, '1', NOW());
/*!40000 ALTER TABLE `SysPlan` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SysAccount` WRITE;
/*!40000 ALTER TABLE `SysAccount` DISABLE KEYS */;
INSERT INTO `SysAccount` (`stHash`, `SysAccount_id`, `SysPlan_id`, `stAccountName`, `enumType`, `SysUser_id`, `SysPerson_id`, `isSystem`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_account_a', 1, 1, 'Onion', 'user', 1, 1, '1', 0, '1', NOW());
/*!40000 ALTER TABLE `SysAccount` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SysUserGroup` WRITE;
/*!40000 ALTER TABLE `SysUserGroup` DISABLE KEYS */;
INSERT INTO `SysUserGroup` (`stHash`, `SysAccount_id`, `SysUserOwner_id`, `stName`, `stLabel`, `txtGroupPermission`, `stDashboard`, `stMenu`, `isSystem`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_user_group_a', 1, 1, 'system', 'System', '{"ALL":{"ALL":"allow"}}', 'admin', 'admin', '1', 0, '1', NOW()),
('sys_user_group_b', 1, 1, 'guest', 'Visitantes', '{"ALL":{"ALL":"deny"}}', NULL, 'gest', '1', 0, '1', NOW()),
('sys_user_group_c', 1, 1, 'admin', 'ADM Sys', '{"ALL":{"ALL":"allow"}}', 'admin', 'admin','1', 0, '1', NOW());
/*!40000 ALTER TABLE `SysUserGroup` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SysPerson` WRITE;
/*!40000 ALTER TABLE `SysPerson` DISABLE KEYS */;
INSERT INTO `SysPerson` (`stHash`, `SysAccount_id`, `SysUserOwner_id`, `stName`, `stCitizenId`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_person_a', 1, 1, 'System', '00000000000', 0, '1', NOW()),
('sys_person_b', 1, 1, 'Administrador', '00000000001', 0, '1', NOW());
/*!40000 ALTER TABLE `SysPerson` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SysUser` WRITE;
/*!40000 ALTER TABLE `SysUser` DISABLE KEYS */;
INSERT INTO `SysUser` (`stHash`, `SysAccount_id`, `SysUserOwner_id`, `SysPerson_id`, `SysUserGroup_id`, `stUsername`, `stPassword`, `stPasswordSalt`, `isSystem`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_user_a', 1, 1, 1, 3, 'system@m3uzz.com', '$2y$10$D5A9Yjk0mTcH.xyrA69Lxeh3ITKU8MJT7EpSaToqdOkOGMAnJToP.', '|#?,4=4;?R?$RS+', '1', 0, '1', NOW()),
('sys_user_b', 1, 1, 2, 3, 'admin@m3uzz.com', '$2y$10$D5A9Yjk0mTcH.xyrA69Lxeh3ITKU8MJT7EpSaToqdOkOGMAnJToP.', '|#?,4=4;?R?$RS+', '1', 0, '1', NOW());
/*!40000 ALTER TABLE `SysUser` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `SysOptions` WRITE;
/*!40000 ALTER TABLE `SysOptions` DISABLE KEYS */;
INSERT INTO `SysOptions` (`stHash`, `SysAccount_id`, `SysUserOwner_id`, `stResource`, `stGroup`, `txtValues`, `stLocale`, `numStatus`, `isActive`, `dtInsert`) VALUES
('sys_options_a', 1, 1, 'SysOptions/SysOptions', 'stLocale', '{\"pt_BR\":{\"value\":\"pt_BR\",\"label\":\"Português do Brasil\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"selected\":true,\"disabled\":false},\"en_US\":{\"value\":\"en_US\",\"label\":\"Inglês Americano\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"selected\":false,\"disabled\":false},\"es_ES\":{\"value\":\"es_ES\",\"label\":\"Espanhol\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"selected\":false,\"disabled\":true}}', 'pt_BR',  0, '1', NOW()),
('sys_options_b', 1, 1, 'SysEntity/Person', 'enumGender', '{\"F\":{\"value\":\"F\",\"label\":\"F\",\"icon\":\"material-icons female\",\"description\":\"Feminino\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"M\":{\"value\":\"M\",\"label\":\"M\",\"icon\":\"material-icons male\",\"description\":\"Masculino\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false}}', 'pt_BR', 0, '1', NOW()),
('sys_options_c', 1, 1, 'SysEntity/Company', 'stArea', '{\"Transportadora\":{\"value\":\"Transportadora\",\"label\":\"Transportadora\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Motorista\":{\"value\":\"Motorista\",\"label\":\"Motorista\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Embarcador\":{\"value\":\"Embarcador\",\"label\":\"Embarcador\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Terminal\":{\"value\":\"Terminal\",\"label\":\"Terminal\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Produtor\":{\"value\":\"Produtor\",\"label\":\"Produtor\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Patio\":{\"value\":\"Patio\",\"label\":\"Patio\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Agente\":{\"value\":\"Agente\",\"label\":\"Agente\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"TI\":{\"value\":\"TI\",\"label\":\"TI\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Telecom\":{\"value\":\"Telecom\",\"label\":\"Telecom\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Logística\":{\"value\":\"Logística\",\"label\":\"Logística\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Educação\":{\"value\":\"Educação\",\"label\":\"Educação\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"selected\":false,\"disabled\":false}}', 'pt_BR', 0, '1', NOW()),
('sys_options_d', 1, 1, 'SysEntity/Contact', 'stLabel', '{\"pessoal\":{\"value\":\"pessoal\",\"label\":\"Pessoal\",\"icon\":\"glyphicon glyphicon-user\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":true,\"disabled\":false,\"hidden\":false},\"profissional\":{\"value\":\"profissional\",\"label\":\"Profissional\",\"icon\":\"glyphicon glyphicon-briefcase\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"contato\":{\"value\":\"contato\",\"label\":\"Contato\",\"icon\":\"glyphicon glyphicon-arrow-right\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false}}', 'pt_BR', 0, '1', NOW()),
('sys_options_e', 1, 1, 'SysEntity/Contact', 'stType', '{\"mobile\":{\"value\":\"mobile\",\"label\":\"Celular\",\"icon\":\"glyphicon glyphicon-phone\",\"description\":\"\",\"placeholder\":\"(  )     -\",\"mask\":\"(99) 9 9999-9999\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"email\":{\"value\":\"email\",\"label\":\"E-mail\",\"icon\":\"glyphicon glyphicon-envelope\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"skype\":{\"value\":\"skype\",\"label\":\"Skype\",\"icon\":\"glyphicon glyphicon-earphone\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"facebook\":{\"value\":\"facebook\",\"label\":\"Facebook\",\"icon\":\"glyphicon glyphicon-thumbs-up\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"phone\":{\"value\":\"phone\",\"label\":\"Telefone fixo\",\"icon\":\"glyphicon glyphicon-phone-alt\",\"description\":\"\",\"placeholder\":\"(  )     -\",\"mask\":\"(99) 9999-9999\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"whatsapp\":{\"value\":\"whatsapp\",\"label\":\"Whatsapp\",\"icon\":\"glyphicon glyphicon-comment\",\"description\":\"\",\"placeholder\":\"(  )       -\",\"mask\":\"(99) 99999-9999\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"linkedin\":{\"value\":\"linkedin\",\"label\":\"Linkedin\",\"icon\":\"glyphicon glyphicon-briefcase\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false}}', 'pt_BR', 0, '1', NOW()),
('sys_options_f', 1, 1, 'SysEntity/Address', 'stPlace', '{\"Trabalho\":{\"value\":\"Trabalho\",\"label\":\"Trabalho\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false},\"Residência\":{\"value\":\"Residência\",\"label\":\"Residência\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":true,\"disabled\":false,\"hidden\":false},\"Escritório\":{\"value\":\"Escritório\",\"label\":\"Escritório\",\"icon\":\"\",\"description\":\"\",\"placeholder\":\"\",\"mask\":\"\",\"class\":\"\",\"style\":\"\",\"pattern\":\"\",\"trigger1\":\"\",\"trigger2\":\"\",\"data\":\"\",\"selected\":false,\"disabled\":false,\"hidden\":false}}', 'pt_BR', 0, '1', NOW());
/*!40000 ALTER TABLE `SysOptions` ENABLE KEYS */;
UNLOCK TABLES;

COMMIT;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*M!100616 SET NOTE_VERBOSITY=@OLD_NOTE_VERBOSITY */;