<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Repository;
use OnionDb\AbstractRepository;


class InstallRepository extends AbstractRepository
{
	/**
	 * 
	 * @param string $psDbName
	 * @return bool
	 */
	public function checkDb (string $psDbName) : bool
	{
		$lsSql = "SHOW DATABASES LIKE '{$psDbName}'";
		
		$laResult = $this->queryExec($lsSql);

		if (!is_array($laResult) || count($laResult) == 0)
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * @param string $psDbName
	 * @return array|bool|null
	 */
	public function createDb (string $psDbName)
	{
		$lsSql = "CREATE DATABASE {$psDbName}";
		
		if (false === $this->create($lsSql))
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * @param string $psDbName
	 * @return array|bool|null
	 */
	public function dropDb (string $psDbName)
	{
		$lsSql = "DROP DATABASE {$psDbName}";
		
		if (false === $this->execute($lsSql))
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 *
	 * @param string $psDbTables
	 * @return array|bool|null
	 */
	public function importDb (string $psDbTables)
	{
		if (false === $this->execute($psDbTables))
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * @param string $psTableName
	 * @return array|bool|null
	 */
	public function createTableBase (string $psTableName)
	{
	    $lsSql = "CREATE TABLE IF NOT EXISTS `{$psTableName}` (
                    `id` int unsigned NOT NULL,
					`stHash` varchar(100) NOT NULL,
					`SysAccount_id` int unsigned NOT NULL,
                    `SysUserOwner_id` int unsigned DEFAULT NULL,
					`txtTag` text,
                    `dtInsert` timestamp NULL DEFAULT NULL,
                    `dtUpdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    `numStatus` tinyint unsigned NOT NULL DEFAULT '0',
                    `isActive` enum('0','1') NOT NULL DEFAULT '1'
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

				  ALTER TABLE `{$psTableName}` ADD PRIMARY KEY (`id`);
				  
				  ALTER TABLE `{$psTableName}` MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

				  ALTER TABLE `{$psTableName}` ADD UNIQUE KEY `stHash` (`stHash`) USING HASH;		 
				  
				  ALTER TABLE `{$psTableName}` ADD KEY `SysAccount_id` (`SysAccount_id`);";
	    
		if (false === $this->create($lsSql))
		{
			return false;
		}
		
		return true;
	}
	
	
	/**
	 * 
	 * @param string $psHost
	 * @param string $psUser
	 * @param string $psPass
	 * @param string $psDb
	 * @param string $psTable
	 * @return bool
	 */
	public function createUser (string $psHost, string $psUser, string $psPass, string $psDb, string $psTable) : bool
	{
		if (empty($psDb))
	    {
	        $psDb = '*';
	    }

	    if (empty($psTable))
	    {
	        $psTable = '*';
	    }
	    
	    if (empty($psHost))
	    {
	        $psHost = '%';
	    }
	    
		$lsSql = "GRANT SELECT, INSERT, UPDATE, DELETE, FILE, INDEX ON {$psDb}.{$psTable} TO '{$psUser}'@'{$psHost}' IDENTIFIED BY '{$psPass}';";

		if ($psUser === 'root')
		{
	    	$lsSql = "GRANT ALL PRIVILEGES ON {$psDb}.{$psTable} TO '{$psUser}'@'{$psHost}' IDENTIFIED BY '{$psPass}';";
		}
		
		if (false === $this->execute($lsSql))
		{
			return false;
		}
		
		return true;
	}


	/**
	 * 
	 * @param array $paConf
	 * @return bool
	 */	
	public function connect (array $paConf = null) : bool
	{
	    return $this->oConnection->connect($paConf);
	}
}
