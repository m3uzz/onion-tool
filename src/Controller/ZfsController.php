<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Controller;
use OnionLib\Prompt;
use OnionLib\System;
use OnionTool\Service\ZfsService;


class ZfsController extends ToolControllerAbstract
{

	/**
	 * 
	 */
	public function init() : void
	{
		$this->checkCli();

		$this->oService = new ZfsService($this);
	}


	/**
	 * 
	 * @return string
	 */
	public function zfsAction() : string
	{
		$loHelp = $this->help(true, false, false);
		$loHelp->setAppVersion("Onion Tool", self::TOOL_VERSION);

		return $loHelp->display();
	}


	/**
	 * 
	 * @return string|null
	 */
	public function prepareAction() : ?string
	{
		if (Prompt::confirm("Create onionTool link on bin folder?")) {
			$this->tool2BinAction();
		}

		if ($this->oService->checkEnv()) {
			if (Prompt::confirm("Install layout dependencies?")) {
				$this->oService->layoutGit();
				$this->oService->layoutDist();
			}
		}

		if (Prompt::confirm("Show Onion Tool help?")) {
			$loHelp = $this->help(true, true, false);

			$loHelp->setAppVersion("Onion Tool", self::TOOL_VERSION);

			return $loHelp->display();
		}

		return null;
	}


	/**
	 *
	 */
	public function tool2BinAction() : void
	{
		$lsOrigem = BIN_DIR . DS . "zfstool.php";
		$lsLink = BASE_DIR . DS . "bin" . DS . "zfstool";
		System::simblink($lsOrigem, $lsLink);

		$loApi = new ApiController(null, $this->oRequest);
		$loApi->tool2BinAction();
	}


	/**
	 * 
	 */
	public function newProjectAction() : void
	{
		$this->oService->newProject();
	}


	/**
	 * 
	 */
	public function newRestfulProjectAction() : void
	{
		$this->setRestfulMod();
		
		$this->oService->newProject();
	}


	/**
	 * 
	 */
	public function newConsoleProjectAction() : void
	{
		$this->setConsoleMod();

		$this->oService->newProject();
	}


	/**
	 * 
	 */
	public function newModuleAction() : void
	{
		$this->oService->newModule();
	}


	/**
	 * 
	 */
	public function newRestfulModuleAction() : void
	{
		$this->setRestfulMod();

		$this->oService->newModule();
	}


	/**
	 * 
	 */
	public function newConsoleModuleAction() : void
	{
		$this->setConsoleMod();

		$this->oService->newModule();
	}


	/**
	 * 
	 */
	public function newControllerAction() : void
	{
		$this->oService->newController();
	}


	/**
	 * 
	 */
	public function newRestfulControllerAction() : void
	{
		$this->setRestfulMod();

		$this->oService->newController();
	}


	/**
	 * 
	 */
	public function newConsoleControllerAction() : void
	{
		$this->setConsoleMod();

		$this->oService->newController();
	}


	/**
	 * 
	 */
	public function newActionAction() : void
	{
		$this->oService->newAction();
	}


	/**
	 * 
	 */
	public function newRestfulActionAction() : void
	{
		$this->setRestfulMod();

		$this->oService->newAction();
	}


	/**
	 * 
	 */
	public function newConsoleActionAction() : void
	{
		$this->setConsoleMod();

		$this->oService->newAction();
	}


	/**
	 * 
	 */
	public function newServiceAction() : void
	{
		$this->oService->newService();
	}


	/**
	 * 
	 */
	public function newRestfulServiceAction() : void
	{
		$this->setRestfulMod();

		$this->oService->newService();
	}


	/**
	 * 
	 */
	public function newConsoleServiceAction() : void
	{
		$this->setConsoleMod();

		$this->oService->newService();
	}


	/**
	 * 
	 */
	public function newHelperAction() : void
	{
		$this->oService->newHelper();
	}


	/**
	 *
	 */
	public function newPluginAction() : void
	{
		$this->oService->newPlugin();
	}


	/**
	 *
	 */
	public function newValidatorAction() : void
	{
		$this->oService->newValidator();
	}


	/**
	 *
	 */
	public function newFactoryAction() : void
	{
		$this->oService->newFactory();
	}


	/**
	 *
	 */
	public function newTestAction() : void
	{
		$this->oService->newTest();
	}


	/**
	 * 
	 */
	public function setFormAction() : void
	{
		$this->oService->setForm();
	}


	/**
	 *
	 */
	public function setGridAction() : void
	{
		$this->oService->setGrid();
	}


	/**
	 * 
	 */
	public function setEntityAction() : void
	{
		$this->oService->setEntity();
	}

	/**
	 * 
	 */
	public function layoutGitAction() : void
	{
		$this->oService->layoutGit();
	}


	/**
	 *
	 */
	public function layoutDistAction() : void
	{
		$this->oService->layoutDist();
	}


	/**
	 *
	 */
	public function installDbAction() : void
	{
		$this->oService->installDb();
	}


	/**
	 * 
	 */
	public function createTableBaseAction() : void
	{
		$this->oService->createTableBase();
	}


	/**
	 * 
	 */
	public function createUserDbAction() : void
	{
		$this->oService->createUserDb();
	}


	/**
	 * 
	 */
	public function confDbAction() : void
	{
		$this->oService->confDb();
	}


	/**
	 * 
	 */
	public function setAuthRolesAction() : void
	{
		$this->oService->setAuthRoles();
	}
}