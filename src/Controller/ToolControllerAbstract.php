<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */
declare (strict_types = 1);

namespace OnionTool\Controller;
use OnionApi\Abstracts\AbstractController;
use OnionHttp\HttpResponse;


abstract class ToolControllerAbstract extends AbstractController
{
	const TOOL_VERSION = "3.20.09";


	/**
	 * 
	 * @return string
	 */
	public function getVersion () : string
	{
		return self::TOOL_VERSION;
	}


	/**
	 * 
	 * @param bool $pbSet
	 */
	public function setRestfulMod (bool $pbSet = true) : void
	{
		$this->oService->setRestfulMod($pbSet);
	}


	/**
	 * 
	 * @param bool $pbSet
	 */
	public function setConsoleMod (bool $pbSet = true) : void
	{
		$this->oService->setConsoleMod($pbSet);
	}


	/**
	 * 
	 * @param string $psAction
	 */
	public function setAction (string $psAction) : void
	{
		$this->sAction = $psAction;
	}


	/**
	 * 
	 */
	public function checkCli () : void
	{
		if (PHP_SAPI != "cli")
	    {    		
    		$laBody = [
    				'status' => 'fail',
    				'statusMessage' => [
    					[
    						'method' => $this->oRequest->getMethod(),
    						'code' => '403',
    						'message' => ['ACCESS FORBIDDEN']
    					]
    				],
    				'data' => []
    		];
			
			$this->oResponse = new HttpResponse(403);
    		$this->oResponse->withJson(($laBody));
    		//$this->oResponse->render();   		
	    }	    
	}


	/**
	 * 
	 * @return string
	 */
	public function showVHostConfAction () : string
	{
		$loHelp = $this->oService->showVHostConf();

		return $loHelp->display();
	}
	

	/**
	 * 
	 */
	public function virtualHostDevAction () : void
	{
		$this->oService->virtualHostDev($this->sController);
	}
	
	
	/**
	 * 
	 */
	public function uninstallProjectAction () : void
	{
		$this->oService->uninstallProject($this->sController);
	}

    
    /**
     */
    public function checkEnvAction () : void
    {
		$this->oService->checkEnv();
	}
}