<?php
/**
 * This file is part of Onion Tool
 *
 * Copyright (c] 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION] HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE] ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionTool
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-tool
 */

return [
	"onionTool" => [
		"api" => [
			"newProject" => [
				"desc" => "Create a new project structure folder",
				"params" => [
					"project" => "New project folder name",
					"module" => "New module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
					"vHost" => "Try create virtual host conf to Apache",
					"force" => "No check root permission",
					"apacherestart" => "Restart Apache",
				],
			],
			"newModule" => [
				"desc" => "Create a new module for a existent project",
				"params" => [
					"project" => "Project folder name",
					"module" => "New module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newController" => [
				"desc" => "Create a new controller for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"controller" => "New controller name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newService" => [
				"desc" => "Create a new service for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"service" => "New service name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"setEntity" => [
				"desc" => "Set module entity as table structure",
				"params" => [
					"project" => "Project folder name",
				    "module" => "Module name",
				    "driver" => "Database driver",
				    "charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
				    "table" => "Database database table name",
				],
			],		        
			"virtualHostDev" => [
				"desc" => "Configure Apache2 virutal hosts for the dev environment",
				"params" => [
					"force" => "No check root permission",
					"project" => "Project folder name",
					"domain" => "Domain for the virtual host access",
					"httpport" => "Apache port to access virtual host",
					"docroot" => "Apache document root folder",
					"hosts" => "Hosts file path",
					"localhost" => "Localhost IP address",
					"apachedir" => "Apache config path",
					"apachegrp" => "Apache group access",
					"apacherestart" => "Restart Apache",
				],
			],
			"showVHostConf" => [
				"desc" => "Information to set Apache2 and configure the virtual host",
				"params" => [],
			],
			"uninstallProject" => [
				"desc" => "Remove all references about project",
				"params" => [
					"force" => "No check root permission",
					"project" => "Project folder name",
					"domain" => "Domain for the virtual host access",
					"docroot" => "Apache document root folder",
					"apachedir" => "Apache config path",
					"apacherestart" => "Restart Apache",
				],
			],		    
			"checkEnv" => [
				"desc" => "Check system environment to verify dependencies",
				"params" => [],
			],
		],
		"zfs" => [
			"newProject" => [
				"desc" => "Create a new project structure folder",
				"params" => [
					"project" => "Project folder",
					"projectname" => "Project title name",
					"domain" => "Host domain",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
					"vHost" => "Try create virtual host conf to Apache",
					"force" => "No check root permission",
					"apacherestart" => "Restart Apache",
				],
			],
			"newRestfulProject" => [
				"desc" => "Create a new RESTFul project structure folder",
				"params" => [
					"project" => "Project folder",
					"projectname" => "Project title name",
					"domain" => "Host domain",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newConsoleProject" => [
				"desc" => "Create a new Console project structure folder",
				"params" => [
					"project" => "Project folder",
					"projectname" => "Project title name",
					"domain" => "Host domain",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newModule" => [
				"desc" => "Create a new module for a existent project",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newRestfulModule" => [
				"desc" => "Create a new RESTFul module for a existent project",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newConsoleModule" => [
				"desc" => "Create a new Console module for a existent project",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newController" => [
				"desc" => "Create a new controller for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"controller" => "New controller name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newRestfulController" => [
				"desc" => "Create a new RESTFul controller for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"controller" => "New controller name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newConsoleController" => [
				"desc" => "Create a new Console controller for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"controller" => "New controller name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
            "newAction" => [
                "desc" => "Create a new action for a existent controller",
                "params" => [
                    "project" => "Project folder name",
                    "module" => "Module name",
                    "controller" => "Controller name",
                    "action" => "New action name",
                    "title" => "Action title"
                ]
			],
			"newRestfulAction" => [
                "desc" => "Create a new RESTFul action for a existent controller",
                "params" => [
                    "project" => "Project folder name",
                    "module" => "Module name",
					"controller" => "Controller name",
					"method" => "Http method verb",
                    "action" => "New action name",
                    "title" => "Action title"
                ]
			],
			"newConsoleAction" => [
                "desc" => "Create a new Console action for a existent controller",
                "params" => [
                    "project" => "Project folder name",
                    "module" => "Module name",
					"controller" => "Controller name",
					"method" => "Http method verb",
                    "action" => "New action name",
                    "title" => "Action title"
                ]
            ],
			"newService" => [
				"desc" => "Create a new service for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"service" => "New service name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newRestfulService" => [
				"desc" => "Create a new RESTFul service for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"service" => "New service name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newConsoleService" => [
				"desc" => "Create a new Console service for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"service" => "New service name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newHelper" => [
				"desc" => "Create a new view helper for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"helper" => "New helper name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newValidator" => [
				"desc" => "Create a new validator for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"validator" => "New validator name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newPlugin" => [
				"desc" => "Create a new plugin for a existent module",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"plugin" => "New plugin name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newFactory" => [
				"desc" => "Create a new factory for a controller, service, helper or plugin",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"factory" => "New factory name",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"newTest" => [
				"desc" => "Create a new test script for a class",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"testtype" => "Class type to test, controller, service ...",
					"testname" => "Test name, or class name to test",
					"author" => "Author name",
					"email" => "Author e-mail",
					"link" => "Project link or repository",
					"cinit" => "Copyrigth year",
				],
			],
			"layoutDist" => [
				"desc" => "Install layout vendors scripts from distribution link",
				"params" => [],
			],
			"layoutGit" => [
				"desc" => "Install layout vendors scripts from github",
				"params" => [],
			],
			"prepare" => [
				"desc" => "Prepare Onion CMS. Install layout dependencie ",
				"params" => [],
			],
			"installDb" => [
				"desc" => "Configure Onion database for a generic application",
				"params" => [
					"project" => "Project folder name",
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
					"dbtemplate" => "Path to database file template"
				],
			],
			"createTableBase" => [
				"desc" => "Create a base table with some default fields",
				"params" => [
					"project" => "Project folder name",
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
					"table" => "Database table name to create",
				],
			],
			"setEntity" => [
				"desc" => "Set or create a new entity class as table structure",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
					"table" => "Database table name",
				],
			],
			"setForm" => [
				"desc" => "Set or create a new form class as table structure",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"form" => "Form name",
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
					"table" => "Database table name",
				],
			],
			"setGrid" => [
				"desc" => "Set or create a new grid class as table structure",
				"params" => [
					"project" => "Project folder name",
					"module" => "Module name",
					"grid" => "Grid name",
					"searchwindow" => "Is search grid window? [s/n]",
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"dbname" => "Database name",
					"table" => "Database table name",
				],
			],
			"createUserDb" => [
				"desc" => "Generate a sql command to create a new user to access database",
				"params" => [
					"driver" => "Database driver",
					"charset" => "Database charset",
					"host" => "Database server host address",
					"port" => "Database server port",
					"user" => "Database user",
					"pass" => "Database user password",
					"newuser" => "New user for database",
					"newpass" => "New password for the new user",
					"authhost" => "Authorized host to have acess",
					"authdb" => "Authorized database to have access",
					"authtable" => "Authorized specifique table to have access",
				],
			],
			"virtualHostDev" => [
				"desc" => "Configure Apache2 virutal hosts for the dev environment",
				"params" => [
					"force" => "No check root permission",
					"project" => "Project folder name",
					"domain" => "Domain for the virtual host access",
					"httpport" => "Apache port to access virtual host",
					"docroot" => "Apache document root folder",
					"hosts" => "Hosts file path",
					"localhost" => "Localhost IP address",
					"apachedir" => "Apache config path",
					"apachegrp" => "Apache group access",
					"apacherestart" => "Restart Apache",
				],
			],
			"showVHostConf" => [
				"desc" => "Information to set Apache2 and configure the virtual host",
				"params" => [],
			],
			"uninstallProject" => [
				"desc" => "Remove all references about project",
				"params" => [
					"force" => "No check root permission",
					"project" => "Project folder name",
					"domain" => "Domain for the virtual host access",
					"docroot" => "Apache document root folder",
					"apachedir" => "Apache config path",
					"apacherestart" => "Restart Apache",
				],
			],
			"checkEnv" => [
				"desc" => "Check system environment to verify dependencies",
				"params" => [],
			],
		],
	],
];
